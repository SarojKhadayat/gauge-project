# Car Rental Change
Note:
Location EHI-E115CE is not available at Saturday and Sunday, so to avoid error "THE RENTAL STATION IS NOT OPEN ON THIS DAY" from EHI
we modify dynamic Pickup/Dropoff day from table to next Monday. To trigger this action we use additional note in test data table.

tags: car, draft, ehi, change


## As a user, I want to change a car in car rental booking from airport to airport with the same dates and destination

tags: XRAY-3007

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location|Days Until Pickup|Number Of Days|
   |-------------------|--------------------|---------------|----------------|-----------------|--------------|
   |SFO                |SFO                 |               |                |89               |3             |

* Verify at least "2" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK 
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>
* Select the last booked trip
* Change car rental trip
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location|Days Until Pickup|Number Of Days|
   |-------------------|--------------------|---------------|----------------|-----------------|--------------|
   |SFO                |SFO                 |               |                |89               |3             |

* Verify at least "2" car(s) is returned
* Select another car from the search results
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>
* Select the last booked trip
* Verify trip was updated with latest added car

## As a user, I want to change dates in car rental booking from airport to location

tags: XRAY-3017

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location |Days Until Pickup|Number Of Days|
   |-------------------|--------------------|---------------|-----------------|-----------------|--------------|
   |SFO                |                    |               |Chicago N Lasalle|89               |3             |

* Verify at least "1" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>
* Select the last booked trip
* Change car rental trip
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location |Days Until Pickup|Number Of Days|
   |-------------------|--------------------|---------------|-----------------|-----------------|--------------|
   |SFO                |                    |               |Chicago N Lasalle|85               |2             |

* Verify at least "1" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>
* Select the last booked trip
* Verify trip was updated with latest added car

## As a user, I want to change a car rental booking from airport to location for another dates and destination from location to airport

tags: XRAY-3018

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location                  |Days Until Pickup|Number Of Days|Special test data notes                           |
   |-------------------|--------------------|---------------|----------------------------------|-----------------|--------------|--------------------------------------------------|
   |LAX                |                    |               |Chicago Enterprise Location E115CE|85               |8             |Dropoff modified to next Monday if it's Sat or Sun|

* Verify at least "1" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>
* Select the last booked trip
* Change car rental trip
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location                   |Dropoff Location|Days Until Pickup|Number Of Days|
   |-------------------|--------------------|----------------------------------|----------------|-----------------|--------------|
   |                   |SFO                 |Chicago Enterprise Location E11557|                |92               |8             |

* Verify at least "1" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>
* Select the last booked trip
* Verify trip was updated with latest added car
