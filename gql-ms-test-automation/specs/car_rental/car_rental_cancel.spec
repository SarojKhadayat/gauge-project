# Car Rental Cancel Flow

Note:
Location EHI-E115CE is not available at Saturday and Sunday, so to avoid error "THE RENTAL STATION IS NOT OPEN ON THIS DAY" from EHI
we modify dynamic Pickup/Dropoff day from table to next Monday. To trigger this action we use additional note in test data table.

tags: car, draft, ehi, cancel

## As a user, I want to cancel a car rental booking with same pickup/drop off airport locations with diff. pickup/drop off dates

tags: XRAY-2548

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location|Days Until CheckIn|Number Of Days|
   |-------------------|--------------------|---------------|----------------|------------------|--------------|
   |SFO                |SFO                 |               |                |89                |1             |

* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Cancel trip
* Verify the trip was cancelled
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Verify the trip was cancelled

## As a user, I want to cancel a car rental booking with diff. pickup/drop off airport locations with diff. pickup/drop off dates

tags: XRAY-2913

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location|Days Until CheckIn|Number Of Days|
   |-------------------|--------------------|---------------|----------------|------------------|--------------|
   |LAX                |SFO                 |               |                |90                |3             |

* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Cancel trip
* Verify the trip was cancelled
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Verify the trip was cancelled

## As a user, I want to cancel a car rental booking with same pickup/drop off non-airport locations with diff. pickup/drop off dates

tags: XRAY-2914

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location                   |Dropoff Location                  |Days Until CheckIn|Number Of Days|
   |-------------------|--------------------|----------------------------------|----------------------------------|------------------|--------------|
   |                   |                    |Chicago Enterprise Location E11557|Chicago Enterprise Location E11557|99                |30            |

* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Cancel trip
* Verify the trip was cancelled
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Verify the trip was cancelled

## As a user, I want to cancel a car rental booking with diff. pickup/drop off non-airport locations with diff. pickup/drop off dates

tags: XRAY-2915

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location                   |Dropoff Location                  |Days Until CheckIn|Number Of Days|Special test data notes                          |
   |-------------------|--------------------|----------------------------------|----------------------------------|------------------|--------------|-------------------------------------------------|
   |                   |                    |Chicago Enterprise Location E115CE|Chicago Enterprise Location E11557|100               |61            |Pickup modified to next Monday if it's Sat or Sun|

* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Cancel trip
* Verify the trip was cancelled
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Verify the trip was cancelled

## As a user, I want to cancel a car rental booking with pickup on non-airport location and drop off on airport location with diff. pickup/drop off dates

tags: XRAY-2916

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location                   |Dropoff Location|Days Until CheckIn|Number Of Days|Special test data notes                          |
   |-------------------|--------------------|----------------------------------|----------------|------------------|--------------|-------------------------------------------------|
   |                   |LAX                 |Chicago Enterprise Location E115CE|                |92                |7             |Pickup modified to next Monday if it's Sat or Sun|

* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Cancel trip
* Verify the trip was cancelled
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Verify the trip was cancelled

## As a user, I want to cancel a car rental booking with pickup on airport location and drop off on non-airport location with diff. pickup/drop off dates

tags: XRAY-2917

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location |Days Until CheckIn|Number Of Days|
   |-------------------|--------------------|---------------|-----------------|------------------|--------------|
   |SFO                |                    |               |Chicago N Lasalle|91                |5             |

* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Cancel trip
* Verify the trip was cancelled
* Get upcoming trips MOCK
<< TODO - REMOVE THIS ^^^ TEMPORARY STEP IS FOR CALL TO MOCKS, LATER WILL USE A STEP FROM COMMON >>* Select the last booked trip
* Verify the trip was cancelled
