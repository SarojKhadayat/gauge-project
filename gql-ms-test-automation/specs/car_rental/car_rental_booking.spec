# Car Rental Booking Flow
Note:
Location EHI-E115CE is not available at Saturday and Sunday, so to avoid error "THE RENTAL STATION IS NOT OPEN ON THIS DAY" from EHI
we modify dynamic Pickup/Dropoff day from table to next Monday. To trigger this action we use additional note in test data table.

tags: car, draft, ehi, booking


## As a user, I want to checkout a car rental with same pickup/drop-off airport location

tags: XRAY-2918

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location|Days Until Pickup|Number Of Days|
   |-------------------|--------------------|---------------|----------------|-----------------|--------------|
   |SFO                |SFO                 |               |                |89               |3             |

* Verify at least "1" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful

## As a user, I want to checkout a car rental with different pickup/drop-off airport locations

tags: XRAY-2919

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location|Days Until Pickup|Number Of Days|
   |-------------------|--------------------|---------------|----------------|-----------------|--------------|
   |LAX                |SFO                 |               |                |90               |3             |

* Verify at least "1" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful

## As a user, I want to checkout a car rental with pickup in airport and drop-off in non-airport location

tags: XRAY-2920

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location |Days Until Pickup|Number Of Days|
   |-------------------|--------------------|---------------|-----------------|-----------------|--------------|
   |SFO                |                    |               |Chicago N Lasalle|91               |5             |

* Verify at least "1" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful

## As a user, I want to checkout a car rental with pickup in non-airport location and drop-off in airport

tags: XRAY-292

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location                   |Dropoff Location|Days Until Pickup|Number Of Days|Special test data notes                          |
   |-------------------|--------------------|----------------------------------|----------------|-----------------|--------------|-------------------------------------------------|
   |                   |LAX                 |Chicago Enterprise Location E115CE|                |85               |8             |Pickup modified to next Monday if it's Sat or Sun|

* Verify at least "1" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful

## As a user, I want to checkout a car rental with same pickup/drop-off non-airport location

tags: XRAY-2922

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location                   |Dropoff Location                  |Days Until Pickup|Number Of Days|Special test data notes                                             |
   |-------------------|--------------------|----------------------------------|----------------------------------|-----------------|--------------|--------------------------------------------------------------------|
   |                   |                    |Chicago Enterprise Location E115CE|Chicago Enterprise Location E115CE|92               |8             |Pickup and drop-off dates modified to next Monday if it's Sat or Sun|

* Verify at least "1" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful

## As a user, I want to checkout a car rental with different pickup/drop-off non-airport locations

tags: XRAY-2912

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location                   |Dropoff Location                  |Days Until Pickup|Number Of Days|Special test data notes                                             |
   |-------------------|--------------------|----------------------------------|----------------------------------|-----------------|--------------|--------------------------------------------------------------------|
   |                   |                    |Chicago Enterprise Location E11557|Chicago Enterprise Location E115CE|99               |8             |Pickup and drop-off dates modified to next Monday if it's Sat or Sun|

* Verify at least "1" car(s) is returned
* Select first car from search results and add to cart
* Checkout a car rental
* Verify reservation was successful
