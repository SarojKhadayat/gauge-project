# Car Rental Shopping Flow
Note:
Location EHI-E115CE is not available at Saturday and Sunday, so to avoid error "THE RENTAL STATION IS NOT OPEN ON THIS DAY" from EHI
we modify dynamic Pickup/Dropoff day from table to next Monday. To trigger this action we use additional note in test data table.

tags: car, draft, ehi, shopping


## As a user, I want to checkout a car with same pickup/drop off airport locations with diff. pickup/drop off dates

tags: XRAY-2531

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location|Days Until CheckIn|Number Of Days|
   |-------------------|--------------------|---------------|----------------|------------------|--------------|
   |SFO                |SFO                 |               |                |89                |1             |

* Verify search result includes details for the car rental
* Select first car from search results and add to cart
* Verify that car added to cart
* Get car additional info and validate that car same as picked from search

## As a user, I want to checkout a car with diff. pickup/drop off airport locations with diff. pickup/drop off dates

tags: XRAY-2541

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location|Days Until CheckIn|Number Of Days|
   |-------------------|--------------------|---------------|----------------|------------------|--------------|
   |LAX                |SFO                 |               |                |90                |3             |

* Verify search result includes details for the car rental
* Select first car from search results and add to cart
* Verify that car added to cart
* Get car additional info and validate that car same as picked from search

## As a user, I want to checkout a car with same pickup/drop off non-airport location with diff. pickup/drop off dates

tags: XRAY-2895

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location                   |Dropoff Location                  |Days Until CheckIn|Number Of Days|
   |-------------------|--------------------|----------------------------------|----------------------------------|------------------|--------------|
   |                   |                    |Chicago Enterprise Location E11557|Chicago Enterprise Location E11557|99                |30            |

* Verify search result includes details for the car rental
* Select first car from search results and add to cart
* Verify that car added to cart
* Get car additional info and validate that car same as picked from search

## As a user, I want to checkout a car with diff. pickup/drop off non-airport locations with diff. pickup/drop off dates

tags: XRAY-2896

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location                   |Dropoff Location                  |Days Until CheckIn|Number Of Days|Special test data notes                          |
   |-------------------|--------------------|----------------------------------|----------------------------------|------------------|--------------|-------------------------------------------------|
   |                   |                    |Chicago Enterprise Location E115CE|Chicago Enterprise Location E11557|100               |61            |Pickup modified to next Monday if it's Sat or Sun|

* Verify search result includes details for the car rental
* Select first car from search results and add to cart
* Verify that car added to cart
* Get car additional info and validate that car same as picked from search

## As a user, I want to checkout a car with pickup on non-airport location and drop off on airport location with diff. pickup/drop off dates

tags: XRAY-2897

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location                   |Dropoff Location|Days Until CheckIn|Number Of Days|Special test data notes                          |
   |-------------------|--------------------|----------------------------------|----------------|------------------|--------------|-------------------------------------------------|
   |                   |LAX                 |Chicago Enterprise Location E115CE|                |92                |7             |Pickup modified to next Monday if it's Sat or Sun|

* Verify search result includes details for the car rental
* Select first car from search results and add to cart
* Verify that car added to cart
* Get car additional info and validate that car same as picked from search


## As a user, I want to remove a car from cart after I added it to the cart

tags: XRAY-3015

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data

   |Pickup Airport Code|Dropoff Airport Code|Pickup Location|Dropoff Location|Days Until CheckIn|Number Of Days|
   |-------------------|--------------------|---------------|----------------|------------------|--------------|
   |SFO                |SFO                 |               |                |89                |1             |

* Verify search result includes details for the car rental
* Select first car from search results and add to cart
* Verify that car added to cart
* Remove selected car from cart
* Verify that car was removed from the cart
* Verify cart is empty

