# Car Rental Search for EHI
As a <EHI Direct Connect> customer, I want to search for a car rental
for an "airport" location and return to the same location
for the desired "pickup date/time" and "drop off date/time"

## Search for car rentals with same pickup/drop off airport location and diff. pickup/drop off dates

tags: XRAY-952, car, ehi, search

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals for same pickup/drop off "airport" location based on input data

   |Pickup Airport Code|Dropoff Airport Code|Days Until CheckIn|Number Of Days|
   |-------------------|--------------------|------------------|--------------|
   |SFO                |SFO                 |89                |3             |

* Verify at least "10" search results are returned by default
* Verify the search results includes car rental of brand "National |OR| Enterprise |OR| Alamo"
Verify the search results includes car rental of brand "Enterprise"
* Verify search result includes details for the car rental
  Image, Seats/Passengers, Bags/Luggage, Transmission Type, Air Conditioner,
  Contract Name, Safety Check, Price per day, Estimated Total, Location, Mileage, Car Make Model
  Missing: Supplied By, Doors
* Verify total price calculation, price per day, extra hour charge

## Search for car rentals with same pickup/drop off non airport location and diff. pickup/drop off dates

tags: XRAY-2487, carrental, ehi, search

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals for same pickup/drop off "non airport" location based on input data

   |Pickup Location  |Dropoff Location |Days Until CheckIn|Number Of Days|
   |-----------------|-----------------|------------------|--------------|
   |Chicago N Lasalle|Chicago N Lasalle|89                |3             |

* Verify at least "10" search results are returned by default
* Verify the search results includes car rental of brand "National |OR| Enterprise |OR| Alamo"
* Verify search result includes details for the car rental
  Image, Seats/Passengers, Bags/Luggage, Transmission Type, Air Conditioner,
  Contract Name, Safety Check, Price per day, Estimated Total, Location, Mileage, Car Make Model
  Missing: Supplied By, Doors
* Verify total price calculation, price per day, extra hour charge

## Search for car rentals and verify price details for search results

tags: XRAY-953, carrental, ehi, search, price

* Authenticate the user "fijiweb_amadeus"
* Search for car rentals for same pickup/drop off "airport" location based on input data

   |Pickup Airport Code|Dropoff Airport Code|Days Until CheckIn|Number Of Days|
   |-------------------|--------------------|------------------|--------------|
   |SFO                |SFO                 |89                |2             |

* Verify that a random row in search results includes number of days, cost per day, total cost, taxes and service fees
  Missing: cost per extra day, cost per extra hour
* Verify that a random row in search results has matching number of days, cost per day, taxes, correct calculation for total rental cost and total cost (incl. taxes & fees)
