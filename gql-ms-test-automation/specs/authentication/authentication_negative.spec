# Login, negative authentication flows

## Login failure using bad password

tags: auth, fail

* Authenticate the user "baduser"
* Verify login failure using "bad password"

## Login failure using bad token

tags: auth, fail

* Authenticate the user "phxuser"
* Verify login failure using "bad token"