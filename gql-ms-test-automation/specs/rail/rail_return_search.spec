# Rail Round-trip Search for UK
As a traveller, I want to search for a round trip train ticket in the UK,
between the stations of "origin" and "destination" for a desired "date" and "time"
in the future

Rail Test Site (Site Id: 245025)

UK Train Locations
London Waterloo -> Guildford -> London Waterloo

## Search for rail with origin and destination stations for onward and outward journey for desired date/time and verify departing and arriving leg details

tags: XRAY-2680, rail, uk, search, roundtrip, RM-498(Bug Fix WIP)

* Authenticate the user "fijiweb_rail"
* Search for return rail with origin and destination stations for onward and outward journey with specified date/time based on table data

   |Origin Station|Destination Station|Days Until Departure|Days Until Return|No of Passengers|
   |--------------|-------------------|--------------------|-----------------|----------------|
   |Guildford     |London Waterloo    |15                  |20               |1               |

* Get list of trains
* Verify at least "5" "departing and arriving" trains legs are returned
* Select "random" "departing" train leg
* For a departing train leg verify the response includes required details

* Select "random" "arriving" train leg
* For a arriving train leg verify the response includes required details
* Select "Standard" fare leg
* Create itinerary
* Add rail trip to the itinerary
* Get rail itinerary
* Get stand alone fare conditions
* Verify stand alone fare conditions and tier info for train leg


## As a traveler, I want to see the stop and duration information for my journey so that I can review the schedule manually

tags: XRAY-2762, rail, uk, search, roundtrip, railstop

* Authenticate the user "fijiweb_rail"
* Search for return rail with origin and destination stations for onward and outward journey with specified date/time based on table data

   |Origin Station|Destination Station|Days Until Departure|Days Until Return|No of Passengers|
   |--------------|-------------------|--------------------|-----------------|----------------|
   |Guildford     |London Waterloo    |15                  |20               |1               |

* Get list of trains
* Select "first" "departing" train leg
* Verify the stop information of the rail stops for all the segments
* Verify the duration information of the rail stops for all the segments
* Select "first" "arriving" train leg
* Verify the stop information of the rail stops for all the segments
* Verify the duration information of the rail stops for all the segments