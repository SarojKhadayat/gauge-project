# Rail One-way Booking for UK
As a Traveler, I want to book one-way train ticket with standard fare in the UK 

Rail Test Site (Site Id: 245025)

UK Train Locations
London Waterloo -> Guildford
FareType: Standard

## Book one-way train ticket with standard fare in the UK

tags: XRAY-2643, rail, uk, booking, oneway

* Authenticate the user "fijiweb_rail"
* Search for rail with origin and destination stations for specified date/time based on table data

   |Origin Station   |Destination Station |Days Until Departure|No of Passengers|
   |-----------------|--------------------|--------------------|----------------|
   |London Waterloo  |Guildford           |15                  |1               |

* Get list of trains
* Select "random" "departing" train leg
* Verify at least "5" "departing" trains legs are returned
* Select "Standard" fare leg
* Get rail journey preferences
* Update journey preferences with delivery option
* Create itinerary
* Add rail trip to the itinerary
* Book "rail" trip and verify booking is completed