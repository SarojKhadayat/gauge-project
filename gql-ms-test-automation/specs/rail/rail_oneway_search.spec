# Rail One-way Search for UK
As a traveller, I want to search for a one-way train ticket in the UK,
between the stations of "origin" and "destination" for a desired "date" and "time"
in the future

Rail Test Site (Site Id: 245025)

UK Train Locations
London Waterloo -> Guildford

## Search for rail with origin and destination stations for desired date/time

tags: XRAY-2337, rail, uk, search, oneway

* Authenticate the user "fijiweb_rail"
* Search for rail with origin and destination stations for specified date/time based on table data

   |Origin Station|Destination Station|Days Until Departure|No of Passengers|
   |--------------|-------------------|--------------------|----------------|
   |Guildford     |London Waterloo    |15                  |1               |

* Get list of trains
* Select "random" "departing" train leg
* Verify at least "5" "departing" trains legs are returned

## As a traveler, I want to choose a standard fare one-way train leg and verify details

tags: XRAY-2463, rail, uk, search, oneway

* Authenticate the user "fijiweb_rail"
* Search for rail with origin and destination stations for specified date/time based on table data

   |Origin Station|Destination Station|Days Until Departure|No of Passengers|
   |--------------|-------------------|--------------------|----------------|
   |Guildford     |London Waterloo    |25                  |1               |

* Get list of trains
* Select "random" "departing" train leg
* For a departing train leg verify the response includes required details
* Select "Standard" fare leg
* Create itinerary
* Add rail trip to the itinerary
* Get rail itinerary
* Get stand alone fare conditions
* Verify stand alone fare conditions and tier info for train leg

## As a traveler, I want to to see the rail cards for my rail search in UK

tags: XRAY-2681, rail, uk, search, oneway

* Authenticate the user "fijiweb_rail"
* Get rail cards for my rail journey
* Verify rail cards response includes required details
* Verify rail cards displayed on alphabetical order

## As a traveler, I want to see the stop and duration information for my journey so that I can review the schedule manually

tags: XRAY-2760, rail, uk, search, oneway, railstop

* Authenticate the user "fijiweb_rail"
* Search for rail with origin and destination stations for specified date/time based on table data

   |Origin Station|Destination Station|Days Until Departure|No of Passengers|
   |--------------|-------------------|--------------------|----------------|
   |Guildford     |London Waterloo    |15                  |1               |
* Get list of trains     
* Select "first" "departing" train leg
* Verify the stop information of the rail stops for all the segments
* Verify the duration information of the rail stops for all the segments