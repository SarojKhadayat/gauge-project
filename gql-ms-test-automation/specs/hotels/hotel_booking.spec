# HSS Hotel Booking
Tests hotel bookingr based on following variables:
SearchType: Address/Airport code
Duration (nights): single

Providers for filter search
   |provider  |User                    |
   |----------|------------------------|
   |Apollo    |phxuser                 |
   |Worldspan |fijiweb_worldspan       |

## Book a stay with Apollo GDS hotels -> Cancellable room

tags: XRAY-2013, hotel

* Authenticate the user "phxuser"
* Search for "address" hotels for specified nights based on table data

   |Address                      |Days Until CheckIn|Number Of Days|
   |-----------------------------|------------------|--------------|
   |642 Harrison st san francisco|60                |1             |

* Select "Preferred" hotel
* Select "In-Policy" and "Cancellable" room
* Create itinerary
* Add hotel to the itinerary
* Book "hotel" trip and verify booking is completed

## Book a stay with Worldspan GDS hotels -> Cancellable room

tags: XRAY-2014, hotel, booking, worldspan, HSS

* Authenticate the user "fijiweb_worldspan"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |60                |2             |
* Select "Preferred" hotel
* Select "In-Policy" and "Cancellable" room
* Create itinerary
* Add hotel to the itinerary
* Book "hotel" trip and verify booking is completed

## Book Hotel with cancellable room using gazoo in apollo gds

tags: XRAY-2013, hotel

* Authenticate the user "fijisabre"
* Search for "address" hotels for specified nights based on table data

   |Address                      |Days Until CheckIn|Number Of Days|
   |-----------------------------|------------------|--------------|
   |642 Harrison st san francisco|60                |1             |

* Create itinerary
* Select cancellable room
* Add cancellable room to itinerary 
* Book hotel and verify booking

## Book a stay with Booking.com ancillary hotel

tags: XRAY-2019, booking.com, HSS, ancillary

* Authenticate the user "fijisabre"
* Search for "address" hotels for specified nights based on table data

   |Address                                       |Days Until CheckIn|Number Of Days|
   |----------------------------------------------|------------------|--------------|
   |Deem Inc - 642 Harrison, San Francisco, CA, US|30                |2             |

* Create itinerary
* Add "cancellable" room for source "booking.com" to the itinerary
* Book "hotel" trip and verify booking is completed

## Book a stay with Tripsource ancillary hotel

tags: XRAY-2017, HSS, hotels, tripsource, ancillary

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |60                |1             |

* Create itinerary
* Add "cancellable" room for source "booking.com" to the itinerary
* Book "hotel" trip and verify booking is completed

## Book a stay with Tripsource ancillary hotel (multiple nights)

tags: XRAY-2803, HSS, hotels, tripsource, ancillary

* Authenticate the user "fijisabre"
* Search for "address" hotels for specified nights based on table data

   |Address                                       |Days Until CheckIn|Number Of Days|
   |----------------------------------------------|------------------|--------------|
   |Deem Inc - 642 Harrison, San Francisco, CA, US|60                |3             |

* Create itinerary
* Add "penalty" room for source "Tripsource" to the itinerary
* Book "hotel" trip and verify booking is completed

## Cancel the upcoming booking.

tags: XRAY-2784, booking, cancel

* Authenticate the user <User>
* Get upcoming trips
* Select and get the first trip
* Cancel and verify trip (booking) is cancelled


## Book a stay with Amadeus GDS hotels -> Cancellable room

tags: XRAY-2015, hotel

* Authenticate the user "fijiweb_amadeus"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |60                |1             |

* Select "Preferred" hotel
* Select "In-Policy" and "Cancellable" room
* Create itinerary
* Add hotel to the itinerary
* Book "hotel" trip and verify booking is completed

## Book a stay with Sabre GDS hotels -> Cancellable room

tags: XRAY-2011, hotel

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Select "Preferred" hotel
* Select "In-Policy" and "Cancellable" room
* Create itinerary
* Add hotel to the itinerary
* Book "hotel" trip and verify booking is completed