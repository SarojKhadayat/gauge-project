# HSS Hotel Search Filter: address, single night
Tests hotel search filter based on following variables:
SearchType: Address/Airport code
Duration (nights): single

Providers for filter search
   |provider |
   |---------|
   |Sabre    |
   |worldspan|


## Filter hotels by star rating

tags: XRAY-2210, hotel, search, filter

* Authenticate the user "fijisabre"
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|90                |1             |
* Filter search results by hotels with "4" star rating
* Verify search results only include hotels with "4" star rating

## Filter hotels by hotel name

tags: XRAY-2173, hotel, search, filter

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Filter search results by hotels with name "Grand Hyatt at SFO"
* Verify search results only include hotels with name "Grand Hyatt at SFO"

## Filter hotels by hotel brand

tags: XRAY-2183, hotel, search, filter

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Filter search results by hotels with brand "Best Western"
* Verify search results only include hotels with brand "Best Western"

## Filter hotels by amenity

tags: XRAY-2209, hotel, search, filter

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |
* Filter search results by hotels with amenity "internet"
* Verify search results only include hotels with amenity "internet"

## Filter hotels by not sold out hotels

tags: XRAY-2211, hotel, search, filter

* Authenticate the user "fijisabre"
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|90                |1             |

* Filter search results by hotels that are not sold out
* Verify search results only include hotels that are not sold out

## Filter hotels by distance

tags: XRAY-2212, hotel, search, filter

* Authenticate the user "fijisabre"
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|90                |1             |

* Filter search results by hotels within distance of "10" miles
* Verify search results include hotels within distance of "10" miles

## Filter hotels based on groups and tabs

tags: XRAY-2215, hotel, search, filter

* Authenticate the user "fijiweb_worldspan"
* Search for "address" hotels for specified nights based on table data

   |Address                      |Days Until CheckIn|Number Of Days|
   |-----------------------------|------------------|--------------|
   |642 Harrison st san francisco|90                |1             |
* Filter search results by hotels with group "Non Preferred"
* Verify search results to only include hotels with group "Non Preferred"

## Filter hotels based on excluded keywords

tags: XRAY-2214, hotel, search

* Authenticate the user "phxuser"
* Search for "address" hotels for specified nights based on table data

   |Address                      |Days Until CheckIn|Number Of Days|
   |-----------------------------|------------------|--------------|
   |642 Harrison st san francisco|90                |1             |
* Verify search results exclude keywords based on table data

   |Excluded Keyword|
   |----------------|
   |advance         |
   |advanced        |
   |flexible        |
   |queen           |


## Filter hotels based on include keywords

tags: XRAY-2864, hotel, search

* Authenticate the user "phxuser"
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|90                |1             |
* Verify search results include keywords based on table data

   |Include Keyword|
   |---------------|
   |king           |
   |discount       |

## Filter and verify hotels by partial hotel name

tags: XRAY-2877, hotel, search

* Authenticate the user "fijisabre"
* Search for "address" hotels for specified nights based on table data

   |Address                      |Days Until CheckIn|Number Of Days|
   |-----------------------------|------------------|--------------|
   |642 Harrison st san francisco|90                |1             |

* Filter the search results based on partial hotel name "Hyatt"
* Verify search results only include hotels with partial hotel name "Hyatt"
