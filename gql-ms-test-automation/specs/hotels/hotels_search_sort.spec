# HSS Hotel Search Sort: Airport, single night
Tests hotel search sort based on following variables:
SearchType: Airport code
Duration (nights): single

Providers for search
   |provider|User     |
   |--------|---------|
   |Sabre   |fijisabre|
## Sort hotel search results based on star rating

tags: XRAY-2224, hotel, search, sort

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |
* Sort search results in "descending" order by "star rating"
* Verify search results are sorted in "descending" order by star rating 

## Sort hotel search results based on price

tags: XRAY-2223, hotel, search, sort

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |
* Sort search results in "ascending" order by "price"
* Verify search results are sorted in "ascending" order by price 

## Sort hotel search results based on distance

tags: XRAY-2225, hotel, search, sort

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |
* Sort search results in "ascending" order by "distance"
* Verify search results are sorted in "ascending" order by distance

## Sort hotel search results based on company policy

tags: XRAY-2226, hotel, search, sort

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |
* Sort search results in "descending" order by "policy"
* Verify search results are sorted in "descending" order by company policy

## Sort hotel search results based on preferred hotels

tags: XRAY-2227, hotel, search, sort

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30x                |1             |
* Sort search results in "descending" order by "preference"
* Verify search results are sorted in "descending" order by preference
* Verify "private rate" hotels are sorted by nearest distance and sold out
* Verify "most preferred" hotels are sorted by nearest distance and sold out
* Verify "highly preferred" hotels are sorted by nearest distance and sold out
* Verify "preferred" hotels are sorted by nearest distance and sold out
* Verify "non preferred" hotels are sorted by nearest distance and sold out
