# HSS Hotel Search Result: airport, single night
Verifies hotel search result based on following variables:
SearchType: Address/Airport code
Duration (nights): single

Providers for filter search
   |provider    |User             |
   |------------|-----------------|
   |Sabre       |fijisabre        |
   |Worldspan   |fijiweb_worldspan|
   |Apollo      |phxuser          |
   |Amadeus     |fijiweb_amadeus  |
   |No Ancillary|fiji_no_ancillary|

## Hotel search results has DSM messages

tags: XRAY-2216, hotel, search, dsm

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |
* Verify DSM messages are present in the search results

   |Header           |Text                                                                         |
   |-----------------|-----------------------------------------------------------------------------|
   |marriotthoteljava|March 14----DSM with Java to display if Marriott Hotels are in search results|
   |HotelDSM         |Non Javascript DSM message at Partner Level                                  |

## Hotel search results has hotel images

tags: XRAY-2217, hotel, search, images

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |
* Verify search results have at least "1" hotel with "1" image
* Verify search results have required image attributes for hotels

## Hotel search results has at least one available hotel

tags: XRAY-2854, hotel, search

* Authenticate the user <User>
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|30                |32            |
* Verify search results have at least "1" available hotel room

## Hotel search results has private rate hotels at the top

tags: XRAY-2218, hotel, search

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Verify search results have private rate hotels at the top

## Hotel search results has corporate and agency preferred hotels and non preferred hotels

tags: XRAY-2868, hotel, search

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Verify search results includes hotels of type "all"
* Verify search results include "private" rate hotels
* Verify all rooms in search results have room source

## Hotel search result doesnt have ancillary source rooms

tags: XRAY-2869, hotel, search

* Authenticate the user "fiji_no_ancillary"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Verify the search results does not include hotels with room source "ancillary"

## As a user, I want to verify that search results have only hotel rooms from ancillary source

tags: XRAY-2869, hotel, search

* Authenticate the user "fiji_ancillary"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |
* Verify the search results have hotel rooms from "ancillary" source

## As a user, I want to verify that search results only have non preferred hotels

tags: XRAY-2872, HSS, hotel, search

* Authenticate the user "fiji_non_preferred"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Verify search results includes "Non-Preferred" hotels

## As a user, I want to verify that search results includes all hotels (PR, Preferred & Non Preferred)

tags: XRAY-2870, HSS, hotel, search

* Authenticate the user "fiji_preferred"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |
* Verify search results includes "all" hotels

## As a user, I want to verify that search results includes only private rate hotels

tags: XRAY-2871, HSS, hotel, search

* Authenticate the user "fiji_privaterate"
* Search for "address" hotels for specified nights based on table data

   |Airport Code    |Days Until CheckIn|Number Of Days|
   |----------------|------------------|--------------|
   |The Westin Tokyo|90                |1             |
   
* Verify search results includes "private" rate hotels

## As a user, I want to verify in-policy status showing only for private rate hotels

tags: XRAY-2874, HSS, hotel, search

* Authenticate the user "fiji_prinpolicy"
* Search for "address" hotels for specified nights based on table data

   |Address                                       |Days Until CheckIn|Number Of Days|
   |----------------------------------------------|------------------|--------------|
   |Deem Inc - 642 Harrison, San Francisco, CA, US|90                |1             |

* Verify "private" rate hotels are "In-Policy"

## As a user, I want to verify that search results have in-policy status for private rate and preferred rate hotels

tags: XRAY-2939, hotel, search

* Authenticate the user "fiji_prandpreferred"
* Search for "address" hotels for specified nights based on table data

   |Address                                       |Days Until CheckIn|Number Of Days|
   |----------------------------------------------|------------------|--------------|
   |Deem Inc - 642 Harrison, San Francisco, CA, US|90                |1             |

* Verify "private" rate hotels are "In-Policy"
* Verify "preferred" rate hotels are "In-Policy"

## As a user, I want to verify In-policy status showing for all hotels (PR, Preferred & Non Preferred)

tags: XRAY-2940, HSS, hotel, search

* Authenticate the user "fiji_noprandpreferred"
* Search for "address" hotels for specified nights based on table data

   |Address                                       |Days Until CheckIn|Number Of Days|
   |----------------------------------------------|------------------|--------------|
   |Deem Inc - 642 Harrison, San Francisco, CA, US|90                |1             |

* Verify "all" rate hotels are "In-Policy"

## As a user, I want to verify search results has all rates for all hotels

tags: XRAY-2967, HSS, hotel, search, private, ancillary

* Authenticate the user "fiji_pr_and_ancillary"
* Search for "address" hotels for specified nights based on table data

   |Address                                       |Days Until CheckIn|Number Of Days|
   |----------------------------------------------|------------------|--------------|
   |Deem Inc - 642 Harrison, San Francisco, CA, US|90                |1             |

* Verify search results includes "all" rate hotels
* Verify search results includes "private" rate hotels
* Verify the search results have hotel rooms from "all" source

## As a user, I want to see in-policy status showing for preferred rate hotels and private rate hotels with roomRate <= MaxRate

tags: XRAY-3043, hotels, preferred, private, in-policy

* Authenticate the user "fiji_preferredprh"
* Search for "address" hotels for specified nights based on table data

   |Address                                       |Days Until CheckIn|Number Of Days|
   |----------------------------------------------|------------------|--------------|
   |Deem Inc - 642 Harrison, San Francisco, CA, US|90                |1             |
   
* Verify "private" rate hotels are "In-Policy"
* Verify "preferred" rate hotels are "In-Policy" 
* Verify the search results have room rate less than or equal to the max rate of "350"   

## As a user, I want to verify search results has agency Preferred chain hotels for all sources

tags : XRAY-2968, HSS, hotel, search, ancillary

* Authenticate the user "fiji_agencypreferred"
* Search for "address" hotels for specified nights based on table data

   |Address                                       |Days Until CheckIn|Number Of Days|
   |----------------------------------------------|------------------|--------------|
   |Deem Inc - 642 Harrison, San Francisco, CA, US|90                |1             |

* Verify search results includes "Preferred" hotels
* Verify the search results have hotel rooms from "all" source

## As a user, I want to verify if there is no PRH hotels in search results then all hotels are in-policy

tags: XRAY-3016, in-policy, HSS, hotel, search

* Authenticate the user "fiji_inpolicyall"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |JFK         |30                |1             |

* Verify search results does not include "private" rate hotels
* Verify "all" rate hotels are "In-Policy"
