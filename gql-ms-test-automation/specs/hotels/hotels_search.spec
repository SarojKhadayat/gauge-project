# HSS Hotel Search: airport, non-airport, single night, preferred, cancellable, in-policy with specified GDS hotels
Tests hotel search based on following variables:
SearchType: airport, location
Duration (nights): single, multiple
Number of Guests: 1

Search details table for hotel search
   |provider |User             |
   |---------|-----------------|
   |Apollo   |phxuser          |
   |Sabre    |fijisabre        |
   |Worldspan|fijiweb_worldspan|
   |Amadeus  |fijiweb_amadeus  |

## As a user, I want to search for a preferred hotel, in-policy and cancellable room type by airport code

tags: XRAY-2117, XRAY-2118, XRAY-2202, hotel, preferred, cancellable, in-policy, search, smoke

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Verify the search results include hotels of hotel type "Preferred" with rooms of policy type "In-Policy" and cancellable policy "Cancellable"

## As a user, I want to search for a preferred hotel, in-policy and cancellable room type by address

tags: XRAY-2117, XRAY-2118, XRAY-2202, hotel, preferred, cancellable, in-policy, search, smoke

* Authenticate the user <User>
* Search for "address" hotels for specified nights based on table data

   |Address                      |Days Until CheckIn|Number Of Days|
   |-----------------------------|------------------|--------------|
   |642 Harrison st san francisco|60                |1             |

* Verify the search results include hotels of hotel type "Preferred" with rooms of policy type "In-Policy" and cancellable policy "Cancellable"

## As a user, I want to search for a preferred hotel, in-policy and Penalty room type by airport

tags: XRAY-2228, hotel, preferred, penalty, in-policy, search, smoke

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |1                 |2             |

* Verify the search results include hotels of hotel type "Preferred" with rooms of policy type "In-Policy" and cancellable policy "Penalty"

## As a user, I want to search for a preferred hotel, in-policy and Penalty room type by address

tags: XRAY-2228, hotel, preferred, penalty, in-policy, search, smoke

* Authenticate the user <User>
* Search for "address" hotels for specified nights based on table data

   |Address                      |Days Until CheckIn|Number Of Days|
   |-----------------------------|------------------|--------------|
   |642 Harrison st san francisco|60                |1             |

* Verify the search results include hotels of hotel type "Preferred" with rooms of policy type "In-Policy" and cancellable policy "Penalty"

## As a user, I want to verify in-policy status showing correctly when RoomRate <= Max-Room-Rate

tags: XRAY-2219, hotel, in-policy

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |

* Verify the search results and check room policy status showing in-policy when average room rate is less than or equal to "200"

## As a user, I want to verify OutOfPolicy status showing correctly when RoomRate > PerDiem

tags: XRAY-2222, hotel, in-policy

* Authenticate the user "phxuser"
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|60                |1             |

* Verify the search results and check room policy status showing OOP when RoomRate is greater than "300"

## As a user, I want to verify In-policy status showing correctly when RoomRate <= PerDiem

tags: XRAY-2220, hotel, in-policy

* Authenticate the user "phxuser"
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|60                |1             |

* Verify the search results and check room policy status showing InPolicy when RoomRate is less than or equal to "300"

## As a user, I want to search for a preferred hotels, room source as Booking.com, and cancellable rooms, by airport code

tags: XRAY-2207, hotel, preferred, cancellable, in-policy, search, Booking.com

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Verify the search results include hotels of hotel type "Preferred" with rooms of policy type "In-Policy" and cancellable policy "Cancellable"
* Verify the search results include hotels with room source "Booking.com"

## As a user, I want to search for a preferred hotels, room source as Tripsource, and cancellable rooms, by airport code

tags: XRAY-2206, hotel, tripsource, preferred, cancellable, in-policy, search

* Authenticate the user "fijisabre"
* Search for "address" hotels for specified nights based on table data

   |Address                                       |Days Until CheckIn|Number Of Days|
   |----------------------------------------------|------------------|--------------|
   |Deem Inc - 642 Harrison, San Francisco, CA, US|90                |1             |
* Verify the search results include hotels of hotel type "Preferred" with rooms of policy type "In-Policy" and cancellable policy "Cancellable"
* Verify the search results include hotels with room source "Tripsource"

## As a user I want to search for hotels with Tripsource hotels -> Penalty Room

tags: XRAY-2232, hotel, tripsource, penalty

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code                                  |Days Until CheckIn|Number Of Days|
   |----------------------------------------------|------------------|--------------|
   |SFO                                           |1                 |2             |

* Verify search result includes "Preferred" hotels
* Verify the search results include rooms of policy type "Out-Of-Policy"
* Verify the search results include hotels with room source "Tripsource"
* Verify the search results include "Penalty" rooms

## As a user, I want to search for booking.com hotels with penalty room

tags: XRAY-2233, hotel, booking.com, penalty, HSS 

* Authenticate the user "fijisabre"
* Search for "airport" hotels for specified nights based on table data
   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |1                 |2             |
* Verify search result includes "Preferred" hotels
* Verify the search results include rooms of policy type "Out-of-Policy"
* Verify search result includes "Penalty" rooms for room type "booking.com"

## As a user, I don't want to see negative hotel room rates in search results

tags: XRAY-2785, hotel

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Verify hotel room rates are not negative
## As a user, I want to search for a hotel with same day checkin

tags: XRAY-2484, hotel, HSS

* Authenticate the user <User>
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |0                 |1             |
* Verify search results has hotels with same day checkin

## As a user, I want to see hotel room rate details in the search results

tags: XRAY-2796, hotel, search

* Authenticate the user <User>
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|30                |2             |

* Verify room rate details

## As a user, I want to see hotel reviews

tags: XRAY-2862, HSS, reviews

* Authenticate the user <User>
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|30                |2             |
* Verify hotel reviews are available
