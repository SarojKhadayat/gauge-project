# HSS pwa post booking
Tests hotel postbooking based on following variables:
SearchType: Address/Airport code
Duration (nights): single

Providers for filter search
   |provider |
   |---------|
   |Apollo   |
   |Worldspan|
## Post Booking: As a user I should be able to modify a trip post booking, to replace a hotel with different dates

tags: XRAY-2720, postbooking, PWA

* Authenticate the user "phxuser"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |


* Create itinerary
* Add "cancellable" room of source "Apollo" to the itinerary
* Update trip name
* Book "hotel" trip and verify booking is completed
* Get upcoming trips
* Select and get the trip with name
* Modify trip
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |60                |1             |

* Select a new cancellable room
* Replace the old room with a new room in the itinerary
* Book "hotel" trip and verify booking is completed

## Post Booking: As a user I should be able to modify Booking.com hotel with different dates

tags: XRAY-2722, postbooking, PWA, booking.com

* Authenticate the user "phxuser"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |

* Create itinerary
* Add "cancellable" room of source "Apollo" to the itinerary
* Update trip name
* Book "hotel" trip and verify booking is completed
* Get upcoming trips
* Select and get the trip with name
* Modify trip
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |60                |1             |

* Select a new "cancellable" room for source "booking.com" to the itinerary
* Replace the old room with a new room in the itinerary
* Book "hotel" trip and verify booking is completed

## Post Booking : As a user I should be able to change worldspan provider room with worldspan GDS room with different location

tags: XRAY-2855, Post Booking, HSS

* Authenticate the user "fijiweb_worldspan"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |

* Create itinerary
* Add "cancellable" room of source "Worldspan" to the itinerary
* Update trip name
* Book "hotel" trip and verify booking is completed
* Get upcoming trips
* Select and get the trip with name
* Modify trip
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|30                |1             |

* Select a new cancellable room
* Replace the old room with a new room in the itinerary
* Book "hotel" trip and verify booking is completed

## Post Booking: As a user I should be able to modify TripSource hotel with different dates

tags: XRAY-2721, postbooking, PWA, booking.com

* Authenticate the user "phxuser"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |

* Create itinerary
* Add "cancellable" room of source "Apollo" to the itinerary
* Update trip name
* Book "hotel" trip and verify booking is completed
* Get upcoming trips
* Select and get the trip with name
* Modify trip
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |60                |1             |

* Select a new "cancellable" room for source "Tripsource" to the itinerary
* Replace the old room with a new room in the itinerary
* Book "hotel" trip and verify booking is completed

## Post Booking : As a user, I should be able to modify a trip to replace a hotel room from a worldspan room with a booking.com source room with different locations

tags: XRAY-2911, Post Booking, HSS

* Authenticate the user "fijiweb_worldspan"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |

* Create itinerary
* Add "cancellable" room of source "Worldspan" to the itinerary
* Update trip name
* Book "hotel" trip and verify booking is completed
* Get upcoming trips
* Select and get the trip with name
* Modify trip
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|30                |1             |

* Select a new "cancellable" room for source "booking.com" to the itinerary
* Replace the old room with a new room in the itinerary
* Book "hotel" trip and verify booking is completed

## Post Booking: As a user I should be able to change hotel with Tripsource room and different location

tags: XRAY-2860, postbooking, PWA, tripsource

* Authenticate the user "fijiweb_worldspan"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |

* Create itinerary
* Add "cancellable" room of source "tripsource" to the itinerary
* Update trip name
* Book "hotel" trip and verify booking is completed
* Get upcoming trips
* Select and get the trip with name
* Modify trip
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|60                |1             |

* Select a new "cancellable" room from source "tripsource"
* Replace the old room with a new room in the itinerary
* Book "hotel" trip and verify booking is completed

## Post Booking: As a user I should be able to change Tripsource room with Apollo gds room with different dates

tags: XRAY-2944, postbookings, tripsource

* Authenticate the user "phxuser"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |

* Create itinerary
* Add "cancellable" room of source "tripsource" to the itinerary
* Update trip name
* Book "hotel" trip and verify booking is completed
* Get upcoming trips
* Select and get the trip with name
* Modify trip
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|90                |1             |
* Select a new "cancellable" room from source "Apollo"
* Replace the old room with a new room in the itinerary
* Book "hotel" trip and verify booking is completed

## Post Booking: As a user I should be able to change tripsource room with worldspan room with different date

tags: XRAY-2943, postbooking, PWA, tripsource, worldspan

* Authenticate the user "fijiweb_worldspan"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |

* Create itinerary
* Add "cancellable" room of source "tripsource" to the itinerary
* Update trip name
* Book "hotel" trip and verify booking is completed
* Get upcoming trips
* Select and get the trip with name
* Modify trip
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|90                |1             |
* Select a new "cancellable" room from source "Worldspan"
* Replace the old room with a new room in the itinerary
* Book "hotel" trip and verify booking is completed

## Post Booking: As a user I should be able to change booking.com room with worldspan room with different dates

tags: XRAY-2941, postbookings, booking.com, worldspan

* Authenticate the user "fijiweb_worldspan"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |

* Create itinerary
* Add "cancellable" room of source "booking.com" to the itinerary
* Update trip name
* Book "hotel" trip and verify booking is completed
* Get upcoming trips
* Select and get the trip with name
* Modify trip
* Search for "address" hotels for specified nights based on table data

   |Address |Days Until CheckIn|Number Of Days|
   |--------|------------------|--------------|
   |New York|90                |1             |

* Select a new "cancellable" room from source "Worldspan"
* Replace the old room with a new room in the itinerary
* Book "hotel" trip and verify booking is completed

## Post Booking: As a user I should be able to change booking.com room with apollo room with different dates

tags: XRAY-2945, postbookings, booking.com, apollo

* Authenticate the user "phxuser"
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |30                |1             |

* Create itinerary
* Add "cancellable" room of source "booking.com" to the itinerary
* Update trip name
* Book "hotel" trip and verify booking is completed
* Get upcoming trips
* Select and get the trip with name
* Modify trip
* Search for "airport" hotels for specified nights based on table data

   |Airport Code|Days Until CheckIn|Number Of Days|
   |------------|------------------|--------------|
   |SFO         |90                |1             |

* Select a new "cancellable" room from source "Apollo"
* Replace the old room with a new room in the itinerary
* Book "hotel" trip and verify booking is completed
