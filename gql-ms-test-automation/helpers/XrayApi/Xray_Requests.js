const axiosClient = require('axios').default;
const projectId = process.env.JIRA_PROJ_ID
let xrayGqlUrl = process.env.XRAY_URL;
const config = {
    headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json'
    }
};
let wait = require("../common/wait");
var cache = require('memory-cache');

const red = "\x1b[31m";
const green = "\x1b[32m"
module.exports.postRequest = async (url, postData, config) => {
    try {
        config.headers.Authorization = cache.get('xrayAuthToken');
        config.headers.lcid = Date.now().toString(36);
        let apiResponse = await axiosClient.post(url, postData, config);
        logSuccess(apiResponse);
        return apiResponse
    } catch (error) {
        if (error.response && isFailedApiStatus(error.response.status)) {
            if (error.response.status == 429) {
                console.log(`Error response code: 429 - retrying`)
                await wait.waitUntilTrue(async () => {
                    let time = error.response.headers["retry-After"];
                    setTimeout(() => {
                        console.log(`Done waiting ${time} Seconds`);
                    }, time * 1000);
                    config.headers.Authorization = cache.get('xrayAuthToken');
                    config.headers.lcid = Date.now().toString(36);
                    console.log(`Xray Headers from error of 429: ${JSON.stringify(error.response.headers)}`)
                    let apiResponse;
                    try {
                        apiResponse = await axiosClient.post(url, postData, config);
                    } catch (error) {
                        return false;
                    }
                    return apiResponse;
                }, retries = 4, durationMs = 1000);
            }
            logFailure(error);
        }
        throw error;
    }
}

function isFailedApiStatus(statusCode) {
    let passStatuses = [200, 204]
    return (!passStatuses.includes(statusCode))
}

function logSuccess(apiResp) {
    console.log(`${green}Status:${apiResp.status} ${apiResp.statusText} - Endpoint: ${apiResp.config.url} - GQL Function: ${Object.keys(apiResp.data.data)[0]}`);
}
function logFailure(err) {
    var errEndPoint = `${err.response.config.url}`;
    if (err.response.config.url.includes("/app/graphql") && err.data) {
        errEndPoint = (`${err.response.config.url} - GQL Function: ${Object.keys(err.data)[0]}`);
    }
    console.log(`${red}Status:${err.response.status} - ${err.response.statusText} - Endpoint ${errEndPoint}\n        
        Data:${JSON.stringify(err.response.data)}\n  
        Headers:${JSON.stringify(err.response.headers)}\n
        LCID: ${err.config.headers.lcid}`);
}
module.exports.xrayAuthApi = async () => {
    console.log(`Executing Xray Authentication`)
    return axiosClient({
        url: 'http://xray.cloud.getxray.app/api/v1/authenticate',
        method: 'post',
        data: {
            client_id: process.env.XRAY_ID,
            client_secret: process.env.XRAY_SECRET
        },
    }).then((result) => {
        return result.data
    })
        .catch(function (error) {
            console.log(`${error}. \n Check that you have XRAY_ID and XRAY_SECRET populated in default.properties file :)`);
        });
}
module.exports.getTests = async (testId) => {
    var body = {
        query: `query {
            getTests(jql: "project = '${projectId}' AND issuekey in (${testId})", limit: 100) {
                results {
                    issueId
                    projectId
                }
            }}`,
        variables: ``
    };

    return await this.postRequest(xrayGqlUrl, body, config);
}

module.exports.getTestExecutions = async (executionJiraId) => {
    var body = {
        query: `query{
            
                getTestExecutions(jql:"project = '${projectId}' AND issuekey in (${executionJiraId})", limit: 100) {
                    total
                    start
                    limit        
                    results {
                        issueId
                    
                    }
                }
            
        }`,
        variables: ``
    };
    return await this.postRequest(xrayGqlUrl, body, config);
}

module.exports.getTestRuns = async (ids, testExecIssueIds) => {
    var body = {
        query: `query {
            getTestRuns( testIssueIds: [${ids}], testExecIssueIds: "${testExecIssueIds}" limit: 100 ) {
              results {
                id
                test {
                    issueId
                    jira(fields: ["key"])
                }
                      }          
                }
              }`,
        variables: ``
    };
    return await this.postRequest(xrayGqlUrl, body, config);
}
module.exports.getTestIdsFromTestPlan = async (testPlanId, start) => {
    var body = {
        query: `query {
            getTestPlans(jql: "project = '${projectId}' AND issuekey in (${testPlanId})", limit: 10) {
                total
                start
                limit
                results {
                    issueId
                    tests(limit: 100,start:${start}) {
                        total
                        start
                        limit
                        results {
                            issueId
                            jira(fields: ["key","summary"])
                            
                        }
                    }
                    jira(fields: ["key","summary"])
                }
            }
        }`,
        variables: ``
    };
    let response;
    try {
        response = await this.postRequest(xrayGqlUrl, body, config);
    } catch (error) {
        console.log(`Error from getTestPlans: ${error}`)
    }
    return response;
}

module.exports.updateTestRunStatus = async (id, status) => {
    var body = {
        query: `mutation {updateTestRunStatus( id: "${id}", status: "${status}")}`,
        variables: ``
    };
    return await this.postRequest(xrayGqlUrl, body, config);
}

module.exports.updateTestRunInfo = async (testGuid, comment, executedById, startedOn, finishedOn) => {
    var body = {
        query: `mutation {updateTestRun( id: "${testGuid}", comment: "${comment}", startedOn: "${startedOn}", finishedOn: "${finishedOn}", executedById: "${executedById}") {warnings}}`,
        variables: ``
    };
    return await this.postRequest(xrayGqlUrl, body, config);
}

module.exports.createTestExecution = async (testPlanInfo) => {
    const ids = []
    let envs = process.env.environment.toUpperCase();
    for (let i = 0; i < testPlanInfo.testCaseInfo.length; i++) {
        ids.push('"' + testPlanInfo.testCaseInfo[i].id + '"')
    }
    let date = new Date().toISOString().replace(/[.]\d+/, '');
    var body = {
        query: `mutation {
            createTestExecution(
                testIssueIds: [${ids}]
                testEnvironments: ["${envs}"]
                jira: {
                    fields: { summary: "Test Execution for: ${testPlanInfo.jiraSummary}, [Automation ${date}]",description:"Test Execution created from Automation run created Date: ${date} " project: {key: "${process.env.JIRA_PROJ_ID}"}  }
                }
            ) {
                testExecution {
                    issueId
                    jira(fields: ["key"])
                }
                warnings
                createdTestEnvironments
            }
        }`,
        variables: ``
    };
    return await this.postRequest(xrayGqlUrl, body, config);
}

module.exports.addTestExecutionsToTestPlan = async (issueId, testExecIssueIds) => {
    var body = {
        query: `mutation {
            addTestExecutionsToTestPlan(
                issueId: "${issueId}",
                testExecIssueIds: ["${testExecIssueIds}"]
            ) {
                addedTestExecutions
                warning
            }
        }`,
        variables: ``
    };
    return await this.postRequest(xrayGqlUrl, body, config);
}
