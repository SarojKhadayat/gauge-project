let urlData = require("../../resources/url.json");
let gqlBodyParams = require("../../resources/defaults/car_rental/gql_queries.json");
let bRequest = require("../common/base_request");
let gqlCreateCarRentalSearchVar = require("../../resources/defaults/car_rental/gql_create_car_rental_search_variables.json");

module.exports.createCarRentalSearch = async (pickUpDateTime, dropOffDateTime, pickUpAirport, dropOffAirport, pickUpLocationId, dropOffLocationId) => {
    // when searching by airport code, pass in the airport code as is. No need to pass in LocationIds
    // when searching by city or location, pass in the LocationId (translated from the lat/long coordinates)
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gauge.dataStore.scenarioStore.put("pickUpDateTime", pickUpDateTime);
    gauge.dataStore.scenarioStore.put("dropOffDateTime", dropOffDateTime);
    gauge.dataStore.scenarioStore.put("pickUpAirport", pickUpAirport);
    gauge.dataStore.scenarioStore.put("dropOffAirport", dropOffAirport);
    gauge.dataStore.scenarioStore.put("pickUpLocationId", pickUpLocationId);
    gauge.dataStore.scenarioStore.put("dropOffLocationId", dropOffLocationId);

    gqlCreateCarRentalSearchVar["variables"].input.pickUpDateTime = pickUpDateTime;
    gqlCreateCarRentalSearchVar["variables"].input.dropOffDateTime = dropOffDateTime;
    gqlCreateCarRentalSearchVar["variables"].input.pickUpAirport = pickUpAirport;
    gqlCreateCarRentalSearchVar["variables"].input.dropOffAirport = dropOffAirport;
    gqlCreateCarRentalSearchVar["variables"].input.pickUpLocationId = pickUpLocationId;
    gqlCreateCarRentalSearchVar["variables"].input.dropOffLocationId = dropOffLocationId;
    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };
    var body = {
        query: gqlBodyParams["createCarRentalSearch"],
        variables: gqlCreateCarRentalSearchVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
}