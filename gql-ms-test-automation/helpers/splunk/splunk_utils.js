const axiosClient = require('axios').default;

module.exports.PostLogsToSplunk = async (projectName, splunkauth,runId) => {
    let splunkJsonBody = await buildSplunkBody(projectName,  runId?runId:"");
    var body = {
        "event": splunkJsonBody
    }
    var requestBody = JSON.stringify(body);
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Splunk ${splunkauth}`
        }
    };
    var url = "https://http-inputs-deem.splunkcloud.com/services/collector";
    try {
        var response = await axiosClient.post(url, requestBody, config);
        if (response.data.text == 'Success') {
            console.log(`Successfully posted results to Splunk for Project Name: ${projectName}`)
        }
        else { console.log(`Didnt post results to Splunk for Project Name: ${projectName}`) }
    }
    catch (error) {
        console.log(`Splunk Logging Error ${JSON.stringify(error)}`)
    }
}

async function buildSplunkBody(projectName,runId) {
    let jsonReport = require("../../reports/json-report/result.json");
    var SplunkBody = {
        "projectName": projectName,
        "runId" : runId,
        "timestamp": jsonReport.timestamp,
        "environment": jsonReport.environment,
        "executionTime": jsonReport.executionTime,
        "executionStatus": jsonReport.executionStatus,
        "passedSpecsCount": jsonReport.passedSpecsCount,
        "failedSpecsCount": jsonReport.failedSpecsCount,
        "skippedSpecsCount": jsonReport.skippedSpecsCount,
        "passedScenariosCount": jsonReport.passedScenariosCount,
        "failedScenariosCount": jsonReport.failedScenariosCount,
        "skippedScenariosCount": jsonReport.skippedScenariosCount,
        "specResults": await buildSpecResults(jsonReport)
    }
    return SplunkBody;
}
async function buildSpecResults(jsonReport) {
    var SpecResults = [];
    for (let spec of jsonReport.specResults) {
        var sec = {
            "specHeading": spec.specHeading,
            "fileName": spec.fileName,
            "tags": spec.tags,
            "executionTime": spec.executionTime,
            "executionStatus": spec.executionStatus,
            "scenarios": await buildSpecResultScenarios(spec.scenarios)
        }
        SpecResults.push(sec);
    }
    return SpecResults;
}
async function buildSpecResultScenarios(Scenarios) {
    var ScenList = [];
    for (let scenario of Scenarios) {
        var sce = {
            "scenarioHeading": scenario.scenarioHeading,
            "tags": scenario.tags,
            "executionTime": scenario.executionTime,
            "executionStatus": scenario.executionStatus,
            "items": await buildScenariosItems(scenario.items)
        }
        ScenList.push(sce);
    }
    return ScenList;
}

async function buildScenariosItems(Items) {
    var ItList = [];
    for (let item of Items) {
        if (item.conceptStep != null) {
            for (let it of item.items) {
                if (it.result.status == "failed") {
                    var itemResult = {
                        "stepText": it.stepText,
                        "status": it.result.status,
                        "stackTrace": it.result.stackTrace,
                        "errorMessage": it.result.errorMessage,
                        "itemType": it.itemType
                    };
                    ItList.push(itemResult);
                }

            }
        }
        else {
            if (item.result.status == "failed") {
                var itemResult = {
                    "stepText": item.stepText,
                    "status": item.result.status,
                    "stackTrace": item.result.stackTrace,
                    "errorMessage": item.result.errorMessage,
                    "itemType": item.itemType
                };
                ItList.push(itemResult);
            }
        }
    }
    return ItList;
}

