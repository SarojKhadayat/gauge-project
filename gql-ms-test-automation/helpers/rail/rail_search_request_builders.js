const commonUtils = require("../../helpers/common/common_utils");
let urlData = require("../../resources/url.json");
let gqlBodyParams = require("../../resources/defaults/rail/gql_queries.json");
let bRequest = require("../common/base_request");
let gqlCreateRailSearchVar = require("../../resources/defaults/rail/gql_create_rail_search_variables.json");
let gqlGetRailVar = require("../../resources/defaults/rail/gql_get_rail_variables.json");
let gqlGetRailItineraryVar = require("../../resources/defaults/rail/gql_get_rail_itinerary_variables.json");
let gqlRailJourneyPreferencesVar = require("../../resources/defaults/rail/gql_get_rail_journey_preferences_variables.json");
let gqlUpdateJourneyPreferencesVar = require("../../resources/defaults/rail/get_update_rail_journey_preferences_variables.json");


let gqlAddRailVar = require("../../resources/defaults/rail/gql_add_rail_variables.json");



module.exports.createRailSearch = async (originStation, destStation, departureDate, arrivalDate, noOfPassengers) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    updateCreateRailSearchRequest(0, originStation, destStation, departureDate)

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };

    if(arrivalDate != null){
        const myClonedArray = [];
        gqlCreateRailSearchVar["variables"].input.railLegSearch.map(val => gqlCreateRailSearchVar["variables"].input.railLegSearch.push(Object.assign({}, val)));
        updateCreateRailSearchRequest(1, destStation, originStation, arrivalDate)
    }

    gqlCreateRailSearchVar["variables"].input.numberOfPassengers = noOfPassengers;

    var body = {
        query: gqlBodyParams["createRailSearch"],
        variables: gqlCreateRailSearchVar["variables"]
    };

    return await bRequest.postRequest(url, body, config);
}

async function updateCreateRailSearchRequest(legNo, originStation, destStation, departureDate) {

    let originLatLong = {"lat": commonUtils.getLatLong(originStation).lat, "long": commonUtils.getLatLong(originStation).long};
    let destLatLong = {"lat": commonUtils.getLatLong(destStation).lat, "long": commonUtils.getLatLong(destStation).long};
    let originUniqueCode = commonUtils.getLatLong(originStation).locCode;
    let destUniqueCode = commonUtils.getLatLong(destStation).locCode;

    gqlCreateRailSearchVar["variables"].input.railLegSearch[legNo].originLocation = originLatLong;
    gqlCreateRailSearchVar["variables"].input.railLegSearch[legNo].originUniqueCode = originUniqueCode;
    gqlCreateRailSearchVar["variables"].input.railLegSearch[legNo].destinationLocation = destLatLong;
    gqlCreateRailSearchVar["variables"].input.railLegSearch[legNo].destinationUniqueCode = destUniqueCode;
    gqlCreateRailSearchVar["variables"].input.railLegSearch[legNo].departureDate = departureDate;

}


module.exports.addRail =  async (tripId, itineraryId) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];

    gqlAddRailVar["variables"].input.key = tripId;
    gqlAddRailVar["variables"].input.itineraryId = itineraryId;

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };
    var body = {
        query: gqlBodyParams["addRail"],
        variables: gqlAddRailVar["variables"]
    };

    return await bRequest.postRequest(url, body, config);
}


module.exports.getRail = async (railSearchId, originStation, destStation) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];

    gqlGetRailVar["variables"].input.searchId = railSearchId;
    gqlGetRailVar["variables"].input.originStationFilters = [originStation];
    gqlGetRailVar["variables"].input.destinationStationFilters = [destStation];
    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };
    var body = {
        query: gqlBodyParams["getRail"],
        variables: gqlGetRailVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
}

module.exports.getRailCards = async () => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };
    var body = {
        query: gqlBodyParams["getRailCards"],
    };
    return await bRequest.postRequest(url, body, config);
}

module.exports.getItinerary = async (itineraryId) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gqlGetRailItineraryVar["variables"].itineraryId = itineraryId;
    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };
    var body = {
        query: gqlBodyParams["getItinerary"],
        variables: gqlGetRailItineraryVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
}

module.exports.getRailJourneyPreferences = async (tripId) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];

    //tripID (searchID__journeyID__fareID)
    let outwardJourneyFareDetails = tripId.split('__');
    gqlRailJourneyPreferencesVar["variables"].input.searchId = outwardJourneyFareDetails[0];
    gqlRailJourneyPreferencesVar["variables"].input.outwardJourney.journeyId = outwardJourneyFareDetails[1];
    gqlRailJourneyPreferencesVar["variables"].input.outwardJourney.fareId = outwardJourneyFareDetails[2];

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };
    var body = {
        query: gqlBodyParams["getRailJourneyPreferences"],
        variables: gqlRailJourneyPreferencesVar["variables"]
    };

    return await bRequest.postRequest(url, body, config);
}


module.exports.updateJourneyPreferences = async (tripId, deliveryOptionCode) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];

    //tripID (searchID__journeyID__fareID)
    let outwardJourneyFareDetails = tripId.split('__');
    gqlUpdateJourneyPreferencesVar["variables"].input.searchId = outwardJourneyFareDetails[0];
    gqlUpdateJourneyPreferencesVar["variables"].input.journeyId = outwardJourneyFareDetails[1];
    gqlUpdateJourneyPreferencesVar["variables"].input.deliveryOption = deliveryOptionCode;


    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };
    var body = {
        query: gqlBodyParams["getUpdateRailJourneyPreferences"],
        variables: gqlUpdateJourneyPreferencesVar["variables"]
    };

    return await bRequest.postRequest(url, body, config);
}

