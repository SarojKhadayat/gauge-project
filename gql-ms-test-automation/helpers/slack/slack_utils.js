const axiosClient = require('axios').default;
module.exports.PostMessageToSlackChannel = async (channel, gitlabUser, pipelineurl, slacktoken, projectname) => {
    console.log(`project name is ${projectname} and Slack channel is:${channel}`)
    let slackJsonBody = await buildSlackResultsBody(channel, gitlabUser, pipelineurl, projectname);
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${slacktoken}`
        }
    };
    var url = "https://slack.com/api/chat.postMessage";
    var response = await axiosClient.post(url, slackJsonBody, config);
    if (!response.data.ok) {
        console.log(`Slack message did not post! error:${response.data.error} user${gitlabUser} channel${channel} and request ${JSON.stringify(slackJsonBody)}`)
    }
}
async function buildSlackSectionBlocksBody(jsonReport, sectionBlocks, gitlabUser, projectname) {
    var blocks = [];
    var header = {
        "type": "header",
        "text": {
            "type": "plain_text",
            "text": `${jsonReport.executionStatus == "passed" ? `:large_green_square:  Success :partying_face:` : `:large_red_square: Failure :disappointed:`}`,
            "emoji": true
        }
    }
    blocks.push(header);
    for (let section of sectionBlocks) {
        var sec = {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": `${section}`
            }
        }
        blocks.push(sec);
    }

    var footer = {
        "type": "context",
        "elements": [
            {
                "type": "mrkdwn",
                "text": `*Build triggered by:* ${gitlabUser}`
            }
        ]
    }
    blocks.push(footer);
    return blocks;
}
// These are not used at present - but keeping code incase we want them in the future.
async function buildErrorBlocks(jsonReport, builtSections) {
    var blocks = [];
    for (let section of builtSections) {
        blocks.push(section);
    }
    var div = {
        "type": "divider"
    };
    blocks.push(div)
    var overflow = [];
    var specResults = jsonReport.specResults;
    let v = 0;
    for (let spec of specResults) {
        for (let scenario of spec.scenarios) {
            if (scenario.executionStatus == "failed") {
                for (let item of scenario.items) {
                    if (item.result.status == "failed") {
                        var sec = {
                            "text": {
                                "type": "plain_text",
                                "text": `${scenario.scenarioHeading}`,
                                "emoji": true
                            },
                            "value": `value-${v++}`
                        }
                        overflow.push(sec);
                    }
                }
            }
        }
    }
    var errorblock =
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": `*List of Failed Tests:*`
            },
            "accessory": {
                "type": "overflow",
                "options": overflow,
                "action_id": "overflow-action"
            }
        };
    blocks.push(errorblock);
    return blocks;
}
async function buildSlackResultsBody(channel, gitlabUser, pipelineurl, projectname) {
    let jsonReport = require("../../reports/json-report/result.json");
    let isPassed = jsonReport.executionStatus == "passed";
    var minutes = Math.floor(jsonReport.executionTime / 60000);
    var seconds = ((jsonReport.executionTime % 60000) / 1000).toFixed(0);
    let executionTime = `${minutes}m ${(seconds < 10 ? "0" : "")}${seconds}s`;
    var sectionBlocks = [];
    sectionBlocks.push(`*Project:* ${projectname} | *Executed:* ${jsonReport.timestamp}`);

    if (!isPassed) {
        sectionBlocks.push(`*Success Rate:* ${successRate = (jsonReport.passedScenariosCount / (jsonReport.failedScenariosCount + jsonReport.passedScenariosCount) * 100).toFixed(2)}%`)
    }
    sectionBlocks.push(`*Test Environment:* ${jsonReport.environment.toUpperCase() == "DEFAULT" ? "SUSTAIN" : jsonReport.environment.toUpperCase()} | *Total time taken:* ${executionTime}`);
    sectionBlocks.push(`*Specifications:* *Passed:* ${jsonReport.passedSpecsCount} | *Failed:* ${jsonReport.failedSpecsCount} | *Skipped:* ${jsonReport.skippedSpecsCount} | *Total:* ${jsonReport.failedSpecsCount + jsonReport.passedSpecsCount + jsonReport.skippedSpecsCount}`);
    sectionBlocks.push(`*Scenarios:* *Passed:* ${jsonReport.passedScenariosCount} | *Failed:* ${jsonReport.failedScenariosCount} | *Skipped:* ${jsonReport.skippedScenariosCount} | *Total:* ${jsonReport.failedScenariosCount + jsonReport.passedScenariosCount + jsonReport.skippedScenariosCount}`);
    sectionBlocks.push(`*Reference:* <${pipelineurl}|HTML Test Report>`);

    var builtSections = await buildSlackSectionBlocksBody(jsonReport, sectionBlocks, gitlabUser, projectname);
    // if (!isPassed) {
    //     sectionBlocks.push(`*Success Rate:* ${successRate = (jsonReport.passedScenariosCount / (jsonReport.failedScenariosCount + jsonReport.passedScenariosCount) * 100).toFixed(2)}%`)
    //     //builtSections = await buildErrorBlocks(jsonReport, builtSections)
    // }
    let slackBody = {
        "channel": `${channel}`,
        "text": `Automation Test Results for ${projectname} at ${jsonReport.timestamp}`,
        "blocks": builtSections
    }
    return slackBody;
}
