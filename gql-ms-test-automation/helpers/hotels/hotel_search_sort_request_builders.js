let urlData = require("../../resources/url.json");
let gqlBodyParams = require("../../resources/defaults/hotel/gql_queries.json");
let bRequest = require("../common/base_request");
let gqlGetHotelVar = require("../../resources/defaults/hotel/gql_get_hotels_variables.json");

module.exports.orderHotelsGql = async (orderBy) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    gqlGetHotelVar["variables"].orderBy=orderBy;
    gqlGetHotelVar["variables"].hotelResultId = gauge.dataStore.scenarioStore.get("hotelResultId");

    var body = {
        query: gqlBodyParams["getHotels"],
        variables: gqlGetHotelVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
}