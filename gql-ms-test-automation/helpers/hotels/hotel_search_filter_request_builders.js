let urlData = require("../../resources/url.json");
let gqlBodyParams = require("../../resources/defaults/hotel/gql_queries.json");
let bRequest = require("../common/base_request");
let gqlGetHotelVar = require("../../resources/defaults/hotel/gql_get_hotels_variables.json");

module.exports.filterHotelsGql = async (filterBy, value) => {
    resetGetHotelVar();
    switch (filterBy) {
        case "starRating":
            gqlGetHotelVar["variables"].starRatings = [parseInt(value)];
            break;
        case "hotelName":
            gqlGetHotelVar["variables"].hotelName = value;
            break;
        case "hotelBrand":
            gqlGetHotelVar['variables'].brandIds = [value];
            break;
        case "amenity":
            gqlGetHotelVar['variables'].amenityIds = [value];
            break; 
        case "hideSoldOut":
            gqlGetHotelVar['variables'].hideSoldOut = value;
            break;
        case "distance":
            gqlGetHotelVar['variables'].distance = parseFloat(value);
            break;  
        case "groupId" :
            gqlGetHotelVar['variables'].groupId = value;    
    }
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    gqlGetHotelVar["variables"].hotelResultId = gauge.dataStore.scenarioStore.get("hotelResultId");

    var body = {
        query: gqlBodyParams["getHotels"],
        variables: gqlGetHotelVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
}

function resetGetHotelVar() {
    gqlGetHotelVar["variables"].hotelName = '';
    gqlGetHotelVar['variables'].starRatings = [];
    gqlGetHotelVar['variables'].brandIds = [];
    gqlGetHotelVar['variables'].amenityIds = [];
    gqlGetHotelVar['variables'].hideSoldOut = false;
    gqlGetHotelVar['variables'].distance = 0;
    gqlGetHotelVar["variables"].groupId = '';

}