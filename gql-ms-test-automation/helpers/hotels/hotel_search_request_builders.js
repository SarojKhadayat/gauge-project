let urlData = require("../../resources/url.json");
let gqlBodyParams = require("../../resources/defaults/hotel/gql_queries.json");
let gqlDefaultBodyParams = require("../../resources/defaults/gql_queries.json");
let bRequest = require("../common/base_request");
let gqlGetHotelVar = require("../../resources/defaults/hotel/gql_get_hotels_variables.json");
let gqlCreateHotelSearchVar = require("../../resources/defaults/hotel/gql_create_hotelsearch_variables.json");
let gqlSelectHotel = require("../../resources/defaults/hotel/gql_select_hotel_variables.json");
let restAddHotel = require("../../resources/defaults/hotel/rest_add_hotel_variables.json");
let gqlReplaceRoom = require("../../resources/defaults/hotel/gql_replace_room_variables.json");
let gqlPostPurchase = require("../../resources/defaults/hotel/gql_post_purchase_variables.json");
let gqlGetRoomDetailsVar = require("../../resources/defaults/hotel/gql_get_roomDetails_variables.json");
let gqlGetHotelReviewsVar = require("../../resources/defaults/hotel/gql_hotel_reviews_variables.json");


module.exports.createHotelSearch = async (lat, lng, checkinDate, checkoutDate, searchType) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gauge.dataStore.scenarioStore.put("lat", lat);
    gauge.dataStore.scenarioStore.put("long", lng);
    gauge.dataStore.scenarioStore.put("checkinDate", checkinDate);
    gauge.dataStore.scenarioStore.put("checkoutDate", checkoutDate);
    if (searchType == "airport") {
        gauge.dataStore.scenarioStore.put("searchBy", 1);
    }
    if (searchType == "address") {
        gauge.dataStore.scenarioStore.put("searchBy", 3);
    }
    gqlCreateHotelSearchVar["variables"].checkIn = checkinDate;
    gqlCreateHotelSearchVar["variables"].checkOut = checkoutDate;
    gqlCreateHotelSearchVar["variables"].latitude = Number(lat);
    gqlCreateHotelSearchVar["variables"].longitude = Number(lng);
    gqlCreateHotelSearchVar["variables"].searchBy = gauge.dataStore.scenarioStore.get("searchBy");
    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {
        query: gqlBodyParams["createHotelSearch"],
        variables: gqlCreateHotelSearchVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
}

module.exports.getHotelsGql = async () => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    gqlGetHotelVar["variables"].hotelResultId = gauge.dataStore.scenarioStore.get("hotelResultId");
    var body = {
        query: gqlBodyParams["getHotels"],
        variables: gqlGetHotelVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
}

module.exports.selectHotel = async (roomId, roomKey) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gqlSelectHotel["variables"].checkIn = gauge.dataStore.scenarioStore.get("checkinDate");
    gqlSelectHotel["variables"].checkOut = gauge.dataStore.scenarioStore.get("checkoutDate");
    gqlSelectHotel["variables"].hotelResultId = gauge.dataStore.scenarioStore.get("hotelResultId");
    gqlSelectHotel["variables"].itineraryId = gauge.dataStore.scenarioStore.get("itineraryId");
    gqlSelectHotel["variables"].hotelId = roomId;
    gqlSelectHotel["variables"].roomKey = roomKey;

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {
        query: gqlBodyParams["selectHotel"],
        variables: gqlSelectHotel["variables"]
    };
    return await bRequest.postRequest(url, body, config);
};
module.exports.restSelectHotel = async (customerId, siteId) => {
    var url = gauge.dataStore.suiteStore.get('baseurl') + urlData["itinerary"] + gauge.dataStore.scenarioStore.get("itineraryId");
    restAddHotel["addHotel"][0].key = gauge.dataStore.scenarioStore.get("roomKey")[0]
    let config = {
        headers: {
            'Accept': '*/*',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'Content-Type': 'application/vnd.reardencommerce.travel.v2.0+json',
            "Accept": 'application/vnd.reardencommerce.travel.v2.0+json',
            'X-Rearden-SecCtx': JSON.stringify({ 'auth': { 'id': customerId, 'type': 'e', 'ext': { 'p': 1, 's': parseInt(siteId) } } })
        }
    };
    var body = {
        addHotel: restAddHotel["addHotel"]
    };
    return await bRequest.postRequest(url, body, config);
};

module.exports.restBookItinerary = async (customerIdOfUser, siteIdOfUser) => {
    var url = gauge.dataStore.suiteStore.get('baseurl') + urlData["itinerary"] + gauge.dataStore.scenarioStore.get("itineraryId");
    restAddHotel["addHotel"][0].key = gauge.dataStore.scenarioStore.get("roomKey")[0]
    let config = {
        headers: {
            'Accept': '*/*',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'Content-Type': 'application/vnd.reardencommerce.travel.v2.0+json',
            "Accept": 'application/vnd.reardencommerce.travel.v2.0+json',
            'X-Rearden-SecCtx': JSON.stringify({ 'auth': { 'id': customerIdOfUser, 'type': 'e', 'ext': { 'p': 1, 's': parseInt(siteIdOfUser) } } })
        }
    };
    var body = {
        addHotel: restAddHotel["addHotel"]
    };
    return await bRequest.postRequest(url, body, config);
};

module.exports.postPurchase = async (bookingId, itineraryId) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gqlPostPurchase["variables"].input.bookingId = bookingId;
    gqlPostPurchase["variables"].input.itineraryId = itineraryId;

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {

        query: gqlDefaultBodyParams["getPostPurchaseInfo"],
        variables: gqlPostPurchase["variables"]
    };
    return await bRequest.postRequest(url, body, config)
};

module.exports.replaceRoom = async (checkinDate, checkoutDate, hotelResultId, itineraryId, hotelId, roomKey, replacedKey) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gqlReplaceRoom["variables"].checkIn = checkinDate;
    gqlReplaceRoom["variables"].checkOut = checkoutDate;
    gqlReplaceRoom["variables"].hotelResultId = hotelResultId;
    gqlReplaceRoom["variables"].itineraryId = itineraryId;
    gqlReplaceRoom["variables"].hotelId = hotelId;
    gqlReplaceRoom["variables"].newRoomKey = replacedKey;
    gqlReplaceRoom["variables"].oldRoomKey = roomKey;
    console.log(`NewRoomKey: ${gqlReplaceRoom["variables"].newRoomKey}- OldRoomKey: ${gqlReplaceRoom["variables"].oldRoomKey}`)
    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {
        query: gqlBodyParams["replaceRoom"],
        variables: gqlReplaceRoom["variables"]
    };
    return await bRequest.postRequest(url, body, config)

};

module.exports.getRoomDetails = async (checkInDate, checkOutDate, resultId, hotelId, roomKey) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gqlGetRoomDetailsVar["variables"].input.checkIn = checkInDate;
    gqlGetRoomDetailsVar["variables"].input.checkOut = checkOutDate;
    gqlGetRoomDetailsVar["variables"].input.hotelId = hotelId;
    gqlGetRoomDetailsVar["variables"].input.hotelResultId = resultId;
    gqlGetRoomDetailsVar["variables"].input.roomId = roomKey;

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {
        query: gqlBodyParams["getHotelRoomDetails"],
        variables: gqlGetRoomDetailsVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
};

module.exports.getHotelReviews = async (hotelId) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gqlGetHotelReviewsVar["variables"].hotelId = hotelId;
    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {

        query: gqlBodyParams["getHotelReviews"],
        variables: gqlGetHotelReviewsVar["variables"]
    };
    return await bRequest.postRequest(url, body, config)
};