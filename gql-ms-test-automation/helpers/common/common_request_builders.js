let gqlDefaultBodyParams = require("../../resources/defaults/gql_queries.json");
let gqlCreateItineraryVar = require("../../resources/defaults/gql_create_itinerary_variables.json");
let gqlBookItineraryVar = require("../../resources/defaults/gql_book_itinerary_variables.json");
let gqlGetOrdersVar = require("../../resources/defaults/gql_get_orders_variables.json");
let gqlGetTripVar = require("../../resources/defaults/gql_get_trip_variables.json");
let gqlCancelTripVar = require("../../resources/defaults/gql_cancel_trip_variables.json");
let bRequest = require("../common/base_request");
let gqlModifyTripVar = require("../../resources/defaults/hotel/gql_modify_trip_variables.json");
let gqlUpdateTripNameVar = require("../../resources/defaults/gql_update_trip_name_variables.json");
let gqlAddToCart = require("../../resources/defaults/gql_add_to_cart_variables.json");
let removeFromCart = require("../../resources/defaults/gql_remove_from_cart_variables.json");
let getCart = require("../../resources/defaults/gql_get_cart_variables.json");
const { getUrl, getBaseConfig } = require("./common_utils");
let urlData = require("../../resources/url.json");

module.exports.createItinerary = async () => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };
    var body = {
        query: gqlDefaultBodyParams["createItinerary"],
        variables: gqlCreateItineraryVar["variables"]
    };

    return await bRequest.postRequest(url, body, config);
}



module.exports.bookItinerary = async (tripType, itineraryId, bookingId, creditCardId) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];

    gqlBookItineraryVar["variables"].input.itineraryId = itineraryId;
    if(bookingId!=''){
        gqlBookItineraryVar["variables"].input.bookingId = bookingId;
    }
    switch (tripType) {
        case 'rail':
            gqlBookItineraryVar["variables"].input.payment.RAIL.creditCard.id = creditCardId;
            break;
        case 'hotel':
            gqlBookItineraryVar["variables"].input.payment.HOTEL.creditCard.id = creditCardId;
            break;
        default:
            return null;
    }

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };
    var body = {
        query: gqlDefaultBodyParams["bookItinerary"],
        variables: gqlBookItineraryVar["variables"]
    };

    return await bRequest.postRequest(url, body, config);
}

module.exports.getOrders = async () => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {
        query: gqlDefaultBodyParams["getOrders"],
        variables: gqlGetOrdersVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
};

module.exports.getTrip = async (selectedTrip, companyId) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gqlGetTripVar["variables"].input.id = selectedTrip.transactionId;
    gqlGetTripVar["variables"].input.transactionGroupId = selectedTrip.transactionGroupId;
    gqlGetTripVar["variables"].input.type = "UPCOMING";
    gqlGetTripVar["variables"].input.companyId = companyId;

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {
        query: gqlDefaultBodyParams["getTrip"],
        variables: gqlGetTripVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
};


module.exports.cancelTrip = async (selectedTrip) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gqlCancelTripVar["variables"].tripId = selectedTrip.processId;

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {
        query: gqlDefaultBodyParams["cancelTrip"],
        variables: gqlCancelTripVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
};

module.exports.modifyTrip = async (processId) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gqlModifyTripVar["variables"].input.processId = processId.toString();

    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {
        query: gqlDefaultBodyParams["reactivateItinerary"],
        variables: gqlModifyTripVar["variables"]
    };
    return await bRequest.postRequest(url, body, config);
};

module.exports.updateTripName = async (name, itineraryId) => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
    gqlUpdateTripNameVar["variables"].input.name = name;
    gqlUpdateTripNameVar["variables"].input.itineraryId = itineraryId;
    let config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')
        }
    };
    var body = {
        query: gqlDefaultBodyParams["updateTripName"],
        variables: gqlUpdateTripNameVar["variables"]
    };
    return await bRequest.postRequest(url, body, config)
};

module.exports.addToCart = async (cartId, intentType, intentDomain, inventoryId, searchId, reservationId) => {
    var url = getUrl();
    gqlAddToCart["variables"].input.cartId = cartId;
    gqlAddToCart["variables"].input.intentDomain = intentDomain;
    gqlAddToCart["variables"].input.intentType = intentType;
    gqlAddToCart["variables"].input.inventoryId = inventoryId;
    gqlAddToCart["variables"].input.searchId = searchId;
    gqlAddToCart["variables"].input.searchId = searchId;
    gqlAddToCart["variables"].input.reservationId = reservationId;
    let config = getBaseConfig();
    var body = {
        query: gqlDefaultBodyParams["addToCart"],
        variables: gqlAddToCart["variables"]
    };
    return await bRequest.postRequest(url, body, config);
}

module.exports.createCartResponse = async () => {
    var url = getUrl();
    let config = getBaseConfig();
    var body = {
        query: gqlDefaultBodyParams["createCartResponse"],
        variables: null
    };
    return await bRequest.postRequest(url, body, config);
}

module.exports.removeFromCart = async (cartId, intentId) => {
    var url = getUrl();
    removeFromCart["variables"].input.cartId = cartId;
    removeFromCart["variables"].input.intentId = intentId;

    let config = getBaseConfig();
    
    var body = {
        query: gqlDefaultBodyParams["removeFromCart"],
        variables: removeFromCart["variables"]
    };
    return await bRequest.postRequest(url, body, config);
}

module.exports.getCart = async (cartId) => {
    var url = getUrl();

    gauge.dataStore.scenarioStore.put("cartId", cartId);

    getCart["variables"].input.cartId = cartId;

    let config = getBaseConfig();

    var body = {
        query: gqlDefaultBodyParams["getCart"],
        variables: getCart["variables"]
    };
    return await bRequest.postRequest(url, body, config);
}