let urlData = require("../../resources/url.json");
let gqlBodyParams = require("../../resources/defaults/gql_queries.json");
let gqlLoginVar = require("../../resources/defaults/gql_login_variables.json");
let bRequest = require("../common/base_request")

module.exports.siteLookUpApi=async(user)=>{
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"]; 
    console.log(`url : ${url}`);
    const config = {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'x-gazoo-env': gauge.dataStore.suiteStore.get('gazooenv')    
        }
    };
    gqlLoginVar["variables"].input.username=user.email;
    gqlLoginVar["variables"].input.password=user.password;
    gqlLoginVar["variables"].input.siteId=user.siteId;
    var body={
      query:gqlBodyParams["loginByCredentials"],
      variables: gqlLoginVar["variables"]
    }
    let siteLookUpApiResponse = await bRequest.postRequest(url, body, config);
    gauge.dataStore.scenarioStore.put('siteLookUpApiResponse', siteLookUpApiResponse.data);
    return siteLookUpApiResponse;
};
module.exports.loginGraphqlApi = async () => {
    var url = gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"]; 
    console.log(`url : ${url}`);
    const config={
        headers:{
            'Accept':'*/*',
            'Content-Type':'application/json',
            'Authorization':gauge.dataStore.scenarioStore.get('authToken'),
            'x-gazoo-env':gauge.dataStore.suiteStore.get('gazooenv')
        }
    }
    var body={
        query:gqlBodyParams["displayConfiguration"]
    }
    let response = await bRequest.postRequest(url, body, config);
    return response;
};