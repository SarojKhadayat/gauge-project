"use strict";
let urlData = require("../../resources/url.json");
let userData = require("../../resources/users.json");
let locatorData = require("../../resources/locator_lat_long.json");
let brandData = require("../../resources/hotel_brands.json");
let groupID = require("../../resources/hotel_groups.json");
var gdsWithNullSource = ["Apollo", "Worldspan"];
module.exports.userDetails = function (userKey) {
    console.log(`Env : ${process.env.environment}`);
    console.log(`Getting data for ${userKey}`);
    let env = userData[process.env.environment];
    return env[userKey];
}

module.exports.calcFutureDate = function (daysCount) {
    var targetDate = new Date();
    targetDate.setDate(targetDate.getDate() + daysCount);
    return `${targetDate.getFullYear()}-${String(targetDate.getMonth() + 1).padStart(2, '0')}-${String(targetDate.getDate()).padStart(2, '0')}`;
}

module.exports.getLatLong = function (locator) {
    return locatorData[locator];
}

module.exports.getBrandId = function (brandName) {
    return brandData[brandName];
}

module.exports.getGroupId = function (groupId) {
    return groupID[groupId];
}
module.exports.isHotelRoomOfPolicy = function (hotelItem, policyType) {
    switch (policyType) {
        case "In-Policy":
            return hotelItem.rooms.filter(roomItem => roomItem.policy.isInPolicy == true);
        case "Out-Of-Policy":
            return hotelItem.rooms.filter(roomItem => roomItem.policy.isInPolicy == false);
        default:
            return null;
    }
}
module.exports.filterHotelsByHotelType = function (hotels, hotelType) {
    switch (hotelType) {
        case "Preferred":
            return hotels.filter(roomItem => roomItem.isPreferred == true);
        case "Non-Preferred":
            return hotels.filter(roomItem => roomItem.isPreferred == false);
        case "all":
            return hotels.map(roomItem => roomItem.isPreferred).flat(1);
        default:
            return null;
    }
}

module.exports.isHotelRoomOfPolicyType = function (hotelItem, roomPolicyType) {
    let isRoomCancellable = roomPolicyType === "Cancellable";
    switch (roomPolicyType) {
        case "Penalty":
            return hotelItem.rooms.filter(roomItem => roomItem.cancellationPolicy === roomPolicyType).length > 0;
        case "Cancellable":
            return hotelItem.rooms.filter(roomItem => roomItem.isFreeCancellable === isRoomCancellable).length > 0;
        default:
            return null;
    }
}

//It allocates numerical value to each preference and sorts to verify preference is in correct order.
module.exports.isInPreferenceOrder = function (sortedHotels, order) {
    let listOfPreference = sortedHotels.map(x => {
        if (x.preference == null) {
            return 0;
        }
        if (x.privateRate != null) {
            return 4;
        }
        switch (x.preference.code) {
            case "MOST_PREFERRED":
                return 3;
            case "HIGHLY_PREFERRED":
                return 2;
            case "PREFERRED":
                return 1;
            default:
                return 0;
        }
    });
    let sortedListOfPreference = [...listOfPreference];
    // sort the list based on asc/desc order as specified
    let orderFunc = order === "ascending" ? (a, b) => a - b : (a, b) => b - a;
    sortedListOfPreference.sort(orderFunc);
    return JSON.stringify(listOfPreference) === JSON.stringify(sortedListOfPreference);
}

//Logic to verify preferred hotels are sorted by nearest distance and not sold hotels are shown first.
module.exports.isPreferredHotelsInOrder = function (preferredHotels) {
    let listOfDistance = preferredHotels.map(x => x.distance.value);
    let sortedListOfDistance = [...listOfDistance];
    sortedListOfDistance.sort((a, b) => a - b);
    let isSortedByNearestDistance = JSON.stringify(listOfDistance) === JSON.stringify(sortedListOfDistance);

    let listOfSoldOut = preferredHotels.map(x => x.isSoldOut);
    let sortedListOfSoldOut = [...listOfSoldOut];
    sortedListOfSoldOut.sort((a, b) => a - b);
    let isSortedBySoldOut = JSON.stringify(listOfSoldOut) === JSON.stringify(sortedListOfSoldOut);

    return isSortedByNearestDistance && isSortedBySoldOut;
}

module.exports.calcDaysBetweenDates = function (endDate, startDate) {
    if (endDate == undefined || endDate == "" || endDate == null || startDate == undefined || startDate == "" || startDate == null) return null;
    let eDate = new Date(endDate);
    let sDate = new Date(startDate);
    return Math.round((eDate - sDate) / 86400000);
}

module.exports.calcRandomInt = function (max) {
    return Math.floor(Math.random() * max);
}

module.exports.isHotelRoomTypeSource = function (hotelItem, roomTypeSource) {
    if (gdsWithNullSource.includes(roomTypeSource)) {
        return hotelItem.rooms.filter(roomItem => roomItem.source == null).length > 0;
    }
    return hotelItem.rooms.filter(roomItem => roomItem.source != null && roomItem.source.toLowerCase() === roomTypeSource.toLowerCase()).length > 0;
}

module.exports.isPrivateRoomAvailable = function (hotels) {
    let privateRooms = hotels.map(x => x.privateRate);
    return privateRooms != null;
}

module.exports.hasValidRateDetails = function (averageRate, totalCost, details) {
    return details != null && details != '' && details.includes(totalCost.amount);
}

module.exports.getSelectedHotelIdAndRoomKey = function (hotels, roomType, roomTypeSource) {
    var hotelData = hotels.filter(hotelItem => hotelItem.rooms.some(room => room.source === roomTypeSource));
    let selectedHotel = {};
    let selectedRoomKey = '';
    if (roomType === 'cancellable') {
        let cancellableHotels = hotelData.filter(x => x.isCancellable);
        selectedHotel = cancellableHotels[0];
        selectedRoomKey = selectedHotel.rooms.filter(x => x.isFreeCancellable)[0].id;
    }
    if (roomType === 'penalty') {
        let penaltyHotel = hotels.filter(x => x.rooms.some(r => r.cancellationPolicy != null && r.cancellationPolicy.toLowerCase() === roomType.toLowerCase()));
        selectedHotel = penaltyHotel[0];
        selectedRoomKey = selectedHotel.rooms.filter(x => x.cancellationPolicy != null && x.cancellationPolicy.toLowerCase() === roomType.toLowerCase())[0].id;
    }
    return [selectedHotel.id, selectedRoomKey];
}

module.exports.generateTripName = function () {
    var now = new Date();
    return `Trip-${now.getFullYear()}${now.getMonth()}${now.getDay()}${now.getHours()}${now.getMinutes()}${now.getSeconds()}${now.getMilliseconds()}`;
}

module.exports.isRoomTypeOfSource = function (room, source) {
    if (gdsWithNullSource.includes(source)) {
        return room.source === null;
    }
    return room.source != null && room.source.toLowerCase() === source.toLowerCase();
}

module.exports.hotelsOfPolicyForRateType = function (hotels, rateType, policyType) {
    switch (rateType) {
        case 'private':
            let privateRateHotels = hotels.filter(x => x.privateRate !== null);
            var hotelsData = privateRateHotels.every(hotelItem =>
                this.isHotelRoomOfPolicy(hotelItem, policyType));
            return hotelsData;
        case "preferred":
            let preferredRateHotels = hotels.filter(x => x.isPreferred == true);
            var preferredRateHotelsData = preferredRateHotels.every(hotelItem =>
                this.isHotelRoomOfPolicy(hotelItem, policyType));
            return preferredRateHotelsData;
        case "all":
            var hotelsData = hotels.filter(hotelItem =>
                this.isHotelRoomOfPolicy(hotelItem,policyType));
                return hotelsData;

        default:
            return null;
    }
}
module.exports.filterHotelsByRateType = function (hotels, rateType) {
    switch (rateType) {
        case 'private':
            let privateRateHotels = hotels.filter(x => x.privateRate !== null);
            return privateRateHotels;
        case "all":
            return hotels;
        default:
            return null;
    }

}

module.exports.filterHotelsByRoomSource = function (allRooms, roomSource) {
    switch (roomSource) {
        case "ancillary":
            return allRooms.filter(roomItem => roomItem.source !== null);
        case "all":
            return allRooms.filter(roomItem => roomItem.source !== null || roomItem.source == null); 
        default:
            return null;
    }
}

module.exports.getUrl = function () {
    return gauge.dataStore.suiteStore.get('gqlurl') + urlData["graphql"];
}

module.exports.getBaseConfig = function () {
    return {
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'Authorization': gauge.dataStore.scenarioStore.get('authToken')
        }
    };
}