let bRequest = require("../common/base_request")
module.exports.waitUntilTrue = waitUntilTrue;

async function waitUntilTrue(condition, onFailAction, retries = 5) {
    let result = false;
    let attempts = 0;
    let elapsed;
    let now;
    let exMessage;
    let exStack;
    let startTime = new Date().getTime();
    let msDuration = process.env.maxWaitMs;

    do {
        try {
            attempts++;
            result = await Promise.resolve(condition());
        } catch (error) {
            exMessage = error.message;
            exStack = error.stack; try {
                if (onFailAction) { onFailAction(); }
            } catch { }
        }
        await forDuration(500);
        now = new Date().getTime();
        elapsed = now - startTime;
    } while (result !== true && elapsed < msDuration && attempts <= retries);
    if (result) {
        return Promise.resolve();
    }
    return Promise.reject(`unable to successfully execute waitUntilTrue(() => {...}) within '${attempts}' attempts due to: '${exMessage}' at: \n${exStack}`)
}

async function forDuration(msDuration) {
    return new Promise((resolve) => {
        setTimeout(resolve, msDuration);
    });
}

module.exports.retryIfInProgress = async (url, body, config) => {
    let response;
    await waitUntilTrue(async () => {
        response = await bRequest.postRequest(url, body, config);
        try {
            if (response.data.errors[0].extensions.code == "IN_PROGRESS") {
                return false;
            }
        } catch (error) {
            return response.data != null;
        }
    });
    return response;
}
