"use strict";

const assert = require("assert");
const { getBrandId, getGroupId } = require("../../helpers/common/common_utils");
const { filterHotelsGql } = require("../../helpers/hotels/hotel_search_filter_request_builders");
var scenarioStore = gauge.dataStore.scenarioStore;

step("Filter search results by hotels with <rate> star rating", async function (rate) {
    let response = await filterHotelsGql("starRating", rate);
    assert(response.data.data && !response.data.errors, `Hotels filtered with star rating ${rate} could not be retrieved.`);
    assert(response.data.data.hotels.totalCount > 0, `No hotels filtered with star rating ${rate} were found.`);
    scenarioStore.put("filteredHotels", response.data.data.hotels.hotels);
});
step("Verify search results only include hotels with <rate> star rating", async function (rate) {
    let searchResults = scenarioStore.get("filteredHotels");
    let filteredHotels = searchResults.filter(x => x.starRating == rate);
    assert(filteredHotels.length > 0 && filteredHotels.length == searchResults.length, `Not all hotels in the filtered list are of ${rate} star rating.`);
});
step("Filter search results by hotels with name <hotelName>", async function (hotelName) {
    let response = await filterHotelsGql("hotelName", hotelName);
    assert(response.data.data && !response.data.errors, `Hotels filtered with name ${hotelName} could not be retrieved.`);
    assert(response.data.data.hotels.totalCount > 0, `No hotels filtered with name ${hotelName} were found.`);
    scenarioStore.put("filteredHotels", response.data.data.hotels.hotels);
});
step("Verify search results only include hotels with name <hotelName>", async function (hotelName) {
    let searchResults = scenarioStore.get("filteredHotels");
    let filteredHotels = searchResults.filter(x => x.name === hotelName);
    assert(filteredHotels.length > 0 && filteredHotels.length == searchResults.length, `Not all hotels in the filtered list are with name ${hotelName}.`);
});

step("Filter search results by hotels with brand <hotelBrand>", async function (hotelBrand) {
    let brandId = getBrandId(hotelBrand);
    let response = await filterHotelsGql("hotelBrand", brandId);
    assert(response.data.data && !response.data.errors, `Hotels filtered with brand ${hotelBrand} could not be retrieved.`);
    assert(response.data.data.hotels.totalCount > 0, `No hotels filtered with brand ${hotelBrand} were found.`);
    scenarioStore.put("filteredHotels", response.data.data.hotels.hotels);
});

step("Verify search results only include hotels with brand <hotelBrand>", async function (hotelBrand) {
    let brandId = getBrandId(hotelBrand);
    let searchResults = scenarioStore.get("filteredHotels");
    let filteredHotels = searchResults.filter(x => x.chainCode === brandId);
    assert(filteredHotels.length > 0 && filteredHotels.length == searchResults.length, `Not all hotels in the filtered list are of brand ${hotelBrand}.`);
});

step("Filter search results by hotels with amenity <amenityId>", async function (amenityId) {
    let response = await filterHotelsGql("amenity", amenityId);
    assert(response.data.data && !response.data.errors, `Hotels filtered with amenity ${amenityId} could not be retrieved.`);
    assert(response.data.data.hotels.hotels.length > 0, `No Hotels filtered with amenity ${amenityId} were found.`);
    scenarioStore.put("filteredHotels", response.data.data.hotels.hotels);
});

step("Verify search results only include hotels with amenity <amenityId>", async function (amenityId) {
    let searchResults = scenarioStore.get("filteredHotels");
    let countAmenityHotel = 0;
    searchResults.map(x => {
        let amenityHotel = x.amenities.filter(item => item.name.toLowerCase() === amenityId && item.isOfProperty);
        if (amenityHotel.length > 0) countAmenityHotel++;
    });
    assert(countAmenityHotel > 0 && countAmenityHotel == searchResults.length, `Not all hotels in the filtered list has amenity ${amenityId}.`);
});

step("Filter search results by hotels that are not sold out", async function () {
    let response = await filterHotelsGql("hideSoldOut", true);
    assert(response.data.data && !response.data.errors, `Hotels filtered to hide sold out could not be retrieved.`);
    scenarioStore.put("filteredHotels", response.data.data.hotels.hotels);
});

step("Verify search results only include hotels that are not sold out", async function () {
    let searchResults = scenarioStore.get("filteredHotels");
    let filteredHotels = searchResults.filter(x => x.isSoldOut == false);
    assert(filteredHotels.length > 0 && filteredHotels.length == searchResults.length, "Some hotels in the filtered list are sold out.");
});

step("Filter search results by hotels within distance of <distance> miles", async function (distance) {
    let response = await filterHotelsGql("distance", distance);
    assert(response.data.data && !response.data.errors, `Hotels filtered within distance ${distance} miles could not be retrieved.`);
    assert(response.data.data.hotels.totalCount > 0, `Hotels filtered within distance ${distance} miles were not found.`);
    scenarioStore.put("filteredHotels", response.data.data.hotels.hotels);
});

step("Verify search results include hotels within distance of <distance> miles", async function (distance) {
    let searchResults = scenarioStore.get("filteredHotels");
    let filteredHotels = searchResults.filter(x => x.distance.value <= Number(distance));
    assert(filteredHotels.length > 0 && filteredHotels.length == searchResults.length, `Not all hotels in the filtered list are within distance ${distance} miles.`);
});

step("Filter search results by hotels with group <group>", async function (group) {
    let groups = getGroupId(group);
    let response = await filterHotelsGql("groupId", groups);
    assert(response.data.data && !response.data.errors, `Hotel search results does not include hotels with group ${group}.`);
    scenarioStore.put("filteredHotels", response.data.data.hotels.hotels);

});

step("Verify search results to only include hotels with group <group>", async function (group) {
    let searchResults = scenarioStore.get("filteredHotels");
    let filteredHotels = searchResults.filter(x => x.preference == null);
    assert(filteredHotels.length == searchResults.length, `Hotel search results does not include hotels with group ${group}.`);
});

step("Verify search results exclude keywords based on table data <tableData>", async function (tableData) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let allRooms = hotels.map(x => x.rooms).flat(1);
    let descriptions = allRooms.map(x => x.description);
    tableData.rows.map(x => assert(!descriptions.some(i => i.includes(x.cells[0])), `Hotel description should not contain excluded keyword ${x.cells[0]}`));
});

step("Verify search results include keywords based on table data <tableData>", async function (tableData) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let descriptions = hotels.map(x => x.rooms).flat(1).map(x => x.description);
    var includeKeywords = tableData.rows.map(x => x.cells).flat(1);
    let isKeywordIncluded = descriptions.every(d => {
        return includeKeywords.some(x => d.includes(x.toUpperCase()));
    });
    assert(isKeywordIncluded, `Hotel description does not contains any of the include keywords ${includeKeywords.join(', ')}.`);
});

step("Filter the search results based on partial hotel name <partialHotelName>", async function(partialHotelName) {
    let response = await filterHotelsGql("hotelName", partialHotelName);
    assert(response.data.data && !response.data.errors, `Hotels filtered with partial hotel name ${partialHotelName} could not be retrieved.`);
    assert(response.data.data.hotels.totalCount > 0, `No hotels filtered with partial hotel name ${partialHotelName} were found.`);
    scenarioStore.put("filteredHotels", response.data.data.hotels.hotels);
});

step("Verify search results only include hotels with partial hotel name <partialHotelName>", async function(partialHotelName) {
    let searchResults = scenarioStore.get("filteredHotels");
    let filteredHotels = searchResults.map(x => x.name);
    assert(filteredHotels.length > 0, `Filtered hotel list is empty.`);
    for(let i=0; i < filteredHotels.length; i++)
    {
        assert(filteredHotels[i].includes(partialHotelName), `Not all hotels from filtered list contains partial hotel name ${partialHotelName}`); 
    }
});