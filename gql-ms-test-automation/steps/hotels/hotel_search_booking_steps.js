"use strict";

const assert = require("assert");
const commonUtils = require("../../helpers/common/common_utils");
var scenarioStore = gauge.dataStore.scenarioStore;
const { selectHotel, restSelectHotel, restBookItinerary } = require("../../helpers/hotels/hotel_search_request_builders");

step("Add <roomType> room of source <roomSource> to the itinerary", async function (roomType, roomSource) {
    let hotelsResult = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let cancellableHotels = hotelsResult.filter(hotelItem => hotelItem.isCancellable && hotelItem.rooms.length > 0 && commonUtils.isHotelRoomTypeSource(hotelItem, roomSource));
    assert(cancellableHotels.length > 0, `The ${roomType} room of source ${roomSource} could not be found.`);
    let selectedHotel = cancellableHotels[0];
    let selectedHotelId = selectedHotel.id;
    let avaliableRooms = selectedHotel.rooms.filter(x => x.isFreeCancellable && commonUtils.isRoomTypeOfSource(x, roomSource));
    assert(avaliableRooms.length > 0, `Cancellable room could not be found for source ${roomSource}.`);
    let selectedRoomKey = avaliableRooms[0].id;
    console.log(selectedRoomKey, "Old room Key");
    let response = await selectHotel(selectedHotelId, selectedRoomKey);
    assert(response.data.data && response.data.data.selectRoom.success, `The ${roomType} room of source ${roomSource} could not be selected.`);
    scenarioStore.put("selectedRoomKey", selectedRoomKey);

});

step("Add <roomType> room for source <roomTypeSource> to the itinerary", async function (roomType, roomTypeSource) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    var hotelData = hotels.filter(hotelItem =>
        commonUtils.isHotelRoomTypeSource(hotelItem, roomTypeSource)
    )
    let cancellableHotels = hotelData.filter(x => x.isCancellable);
    assert(cancellableHotels.length > 0, "Cancellable Room could not be found.");
    let selectedHotel = cancellableHotels[0];
    let selectedRoomId = selectedHotel.id;
    let selectedRoomKey = selectedHotel.rooms.filter(x => x.isFreeCancellable)[0].id;
    let response = await selectHotel(selectedRoomId, selectedRoomKey);
    assert(response.data.data.selectRoom.success, `Cancellable room of type ${roomTypeSource} could not be selected.`);
});
step("Select <hotelType> hotel", async function (hotelType) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let preferredHotels = hotels.filter(hotel => hotel.isPreferred === ("Preferred" === hotelType));
    assert(preferredHotels.length > 0, `${hotelType} hotels were not found.`);
    scenarioStore.put("selectedPreferredHotel", preferredHotels[0]);
});
step("Select <policyType> and <roomType> room", async function (policyType, roomType) {
    let selectedPreferredHotel = scenarioStore.get("selectedPreferredHotel");
    let isInPolicy = policyType === "In-Policy";
    let isCancellable = roomType === "Cancellable";
    let roomsToSelect = selectedPreferredHotel.rooms.filter(room => room.policy.isInPolicy === isInPolicy && room.isFreeCancellable === isCancellable);
    assert(roomsToSelect.length > 0, `${policyType}, ${roomType} rooms were not found.`);
    scenarioStore.put("selectedRoom", roomsToSelect[0]);
});
step("Add hotel to the itinerary", async function () {
    let selectedRoomId = scenarioStore.get("selectedPreferredHotel").id;
    let selectedRoomKey = scenarioStore.get("selectedRoom").id;
    let response = await selectHotel(selectedRoomId, selectedRoomKey);
    assert(response.data.data.selectRoom.success, `Room could not be selected.`);
});
step("Book hotel and verify booking is completed", async function () {
    let userDetails = scenarioStore.get("userDetails");
    let getBookItineraryResponse = await bookItinerary(scenarioStore.get("itineraryId"), userDetails.hotelCreditCardId);
    assert((getBookItineraryResponse.data.data || !getBookItineraryResponse.data.errors), "Hotel booking failed with errors.");
    assert((getBookItineraryResponse.data.data.bookItinerary.success), "Hotel Booking failed and returned no success response");
});

step("Select cancellable room", async function () {
    let hotelsResult = scenarioStore.get("searchedHotels").data.data.hotels.hotels
    let roomId = hotelsResult.map(x => x.id).flat(1);
    scenarioStore.put("roomId", roomId);
    let allRooms = hotelsResult.map(x => x.rooms).flat(1);
    let cancellableHotelRooms = allRooms.filter(x => x.isFreeCancellable === true);
    assert(cancellableHotelRooms.length > 0, `Hotel search result does not contain cancellable hotel rooms.`);
    let roomKey = cancellableHotelRooms.map(x => x.id).flat(1);
    scenarioStore.put("roomKey", roomKey);
});

step("Add cancellable room to itinerary", async function () {
    let userDetails = scenarioStore.get("userDetails")
    let customerId = userDetails["customerId"];
    let siteId = userDetails["siteId"];
    let response = await restSelectHotel(customerId, siteId);
    assert(response, `Cancellable room was not added to the itinerary.`);
    gauge.dataStore.scenarioStore.put('customerId', customerId);
    gauge.dataStore.scenarioStore.put('siteId', siteId);

});

step("Book hotel and verify booking", async function () {
    let customerIdOfUser = gauge.dataStore.scenarioStore.get('customerId');
    let siteIdOfUser = gauge.dataStore.scenarioStore.get('siteId');
    let getBookItineraryResponse = await restBookItinerary(customerIdOfUser, siteIdOfUser);
    assert((getBookItineraryResponse.data.data && !getBookItineraryResponse.data.errors && getBookItineraryResponse.data.data.bookItinerary.success), `Hotel booking failed.`);
});

step("Select a new <roomType> room for source <roomTypeSource> to the itinerary", async function (roomType, roomTypeSource) {
    let hotelsResult = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    var hotelData = hotelsResult.filter(hotelItem =>
        commonUtils.isHotelRoomTypeSource(hotelItem, roomTypeSource)
    )
    let cancellableHotels = hotelData.filter(x => x.isCancellable);
    assert(cancellableHotels.length > 0, "Cancellable hotel could not be found")
    let selectedHotel = cancellableHotels[1];
    scenarioStore.put("replacedHotelId", selectedHotel.id);
    let replacedRoomKey = selectedHotel.rooms.filter(x => x.isFreeCancellable && x.id != scenarioStore.get("selectedRoomKey"))[0].id;
    assert(replacedRoomKey.length > 0, `${roomType} room of type ${roomTypeSource} could not be found.`);
    console.log(replacedRoomKey, "Replaced room key")
    scenarioStore.put("replacedRoomKey", replacedRoomKey);
});

step("Select a new cancellable room", async function () {
    let hotelsResult = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let cancellableHotels = hotelsResult.filter(x => x.isCancellable);
    assert(cancellableHotels.length > 0, "Cancellable hotel could not be found")
    let selectedHotel = cancellableHotels[1];
    scenarioStore.put("replacedHotelId", selectedHotel.id);
    let replacedRoomKey = selectedHotel.rooms.filter(x => x.isFreeCancellable && x.id != scenarioStore.get("selectedRoomKey"))[0].id;
    assert(replacedRoomKey.length > 0, "Cancellable room could not be found");
    console.log(replacedRoomKey, "Replaced room key")
    scenarioStore.put("replacedRoomKey", replacedRoomKey);

});

step("Select a new <roomType> room from source <roomTypeSource>", async function (roomType, roomTypeSource) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    var hotelData = hotels.filter(hotelItem =>
        commonUtils.isHotelRoomTypeSource(hotelItem, roomTypeSource)
    )
    let cancellableHotels = hotelData.filter(x => x.isCancellable);
    assert(cancellableHotels.length > 0, `The ${roomType} hotel could not be found.`);
    let selectedHotel = cancellableHotels[0];
    scenarioStore.put("replacedHotelId", selectedHotel.id);
    let replacedRoomKey = selectedHotel.rooms.filter(x => x.isFreeCancellable && x.id != scenarioStore.get("selectedRoomKey"))[0].id;
    assert(replacedRoomKey.length > 0, `The ${roomType} room could not be found.`);
    console.log(replacedRoomKey, "Replaced room key")
    scenarioStore.put("replacedRoomKey", replacedRoomKey);
});