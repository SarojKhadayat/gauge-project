"use strict";

const assert = require("assert");
const { isPreferredHotelsInOrder, isInPreferenceOrder } = require("../../helpers/common/common_utils");
const { orderHotelsGql } = require("../../helpers/hotels/hotel_search_sort_request_builders");
var scenarioStore = gauge.dataStore.scenarioStore;

step("Sort search results in <order> order by <sortString>", async function (order, sortString) {
    let sortBy = sortString.replace(/ /g, '');  // remove any spaces from sort string
    let getHotelResponse = await orderHotelsGql(sortBy);
    assert(getHotelResponse.data.data && !getHotelResponse.data.errors, `Hotels sorted in ${order} order based on ${sortString} could not be retrieved.`);
    scenarioStore.put("hotelList", getHotelResponse);
});

step("Verify search results are sorted in <order> order by star rating", async function(order){
    var sortedHotels = scenarioStore.get("hotelList").data.data.hotels.hotels;
    let listOfSortByProperty = [];

    // extract the property used for sorting
    listOfSortByProperty = sortedHotels.map(x => x.starRating);
    assert(listOfSortByProperty && listOfSortByProperty.length > 0 && listOfSortByProperty[0], "The sort by property used is not valid.");

    let sortedListOfSortByProperty = [...listOfSortByProperty];
    // sort the list based on asc/desc order as specified
    let orderFunc = order === "ascending" ? (a, b) => a - b : (a, b) => b - a;
    sortedListOfSortByProperty.sort(orderFunc);
    assert(JSON.stringify(listOfSortByProperty) === JSON.stringify(sortedListOfSortByProperty), `The hotel list is not sorted by star rating in ${order} order.`);
});

step("Verify search results are sorted in <order> order by price", async function(order){
    var sortedHotels = scenarioStore.get("hotelList").data.data.hotels.hotels;
    let nonSoldOutHotels = sortedHotels.filter(x => !x.isSoldOut); //SoldOut hotels no rate is showing in the response
    let listOfSortByProperty = [];

    // extract the property used for sorting. make sure the last property used in sequence is valid
    listOfSortByProperty = nonSoldOutHotels.map(x => {
        assert(x.rate, "The hotel does not have a valid rate/price.");
        return x.rate.primary.amount;
    });
    assert(listOfSortByProperty && listOfSortByProperty.length > 0 && listOfSortByProperty[0], "The sort by property used is not valid.");

    let sortedListOfSortByProperty = [...listOfSortByProperty];
    // sort the list based on asc/desc order as specified
    let orderFunc = order === "ascending" ? (a, b) => a - b : (a, b) => b - a;
    sortedListOfSortByProperty.sort(orderFunc);
    assert(JSON.stringify(listOfSortByProperty) === JSON.stringify(sortedListOfSortByProperty), `The hotel list is not sorted by price in ${order} order.`);
});

step("Verify search results are sorted in <order> order by distance", async function(order){
    var sortedHotels = scenarioStore.get("hotelList").data.data.hotels.hotels;
    let listOfSortByProperty = [];

    // extract the property used for sorting. make sure the last property used in sequence is valid
    listOfSortByProperty = sortedHotels.map(x => x.distance.value);
    assert(listOfSortByProperty && listOfSortByProperty.length > 0 && listOfSortByProperty[0], "The sort by property used is not valid.");

    let sortedListOfSortByProperty = [...listOfSortByProperty];
    // sort the list based on asc/desc order as specified
    let orderFunc = order === "ascending" ? (a, b) => a - b : (a, b) => b - a;
    sortedListOfSortByProperty.sort(orderFunc);
    assert(JSON.stringify(listOfSortByProperty) === JSON.stringify(sortedListOfSortByProperty), `The hotel list is not sorted by distance in ${order} order.`);
});

step("Verify search results are sorted in <order> order by company policy", async function(order){
    var sortedHotels = scenarioStore.get("hotelList").data.data.hotels.hotels;
    let listOfSortByProperty = [];

    // extract the property used for sorting. make sure the last property used in sequence is valid
    listOfSortByProperty = sortedHotels.map(x => x.policy.isInPolicy);
    assert(listOfSortByProperty && listOfSortByProperty.length > 0 && listOfSortByProperty[0], "The sort by property used is not valid.");

    let sortedListOfSortByProperty = [...listOfSortByProperty];
    // sort the list based on asc/desc order as specified
    let orderFunc = order === "ascending" ? (a, b) => a - b : (a, b) => b - a;
    sortedListOfSortByProperty.sort(orderFunc);
    assert(JSON.stringify(listOfSortByProperty) === JSON.stringify(sortedListOfSortByProperty), `The hotel list is not sorted by company policy in ${order} order.`);
});


step("Verify search results are sorted in <order> order by preference", async function (order) {
    let sortedHotels = scenarioStore.get("hotelList").data.data.hotels.hotels;
    assert(isInPreferenceOrder(sortedHotels, order), `The hotel list is not sorted by preferences in ${order} order.`);
});

step("Verify <preferenceType> hotels are sorted by nearest distance and sold out", async function (preferenceType) {
    let sortedHotels = scenarioStore.get("hotelList").data.data.hotels.hotels;
    let nonPrivateRatePreferredHotels = sortedHotels.filter(x => x.privateRate == null && x.preference !== null);
    let nonPrivateRateNonPreferredHotels = sortedHotels.filter(x => x.privateRate == null && x.preference == null);

    switch (preferenceType) {
        case "non preferred":
            let nonPreferredHotelsInpolicyNonSoldOutHotels=nonPrivateRateNonPreferredHotels.filter(x => x.policy.isInPolicy && !x.isSoldOut);
            assert(isPreferredHotelsInOrder(nonPreferredHotelsInpolicyNonSoldOutHotels), 'The non preferred Inpolicy hotels are not sorted by nearest distance.');
            let nonPreferredHotelsSoldOutHotels=nonPrivateRateNonPreferredHotels.filter(x => x.policy.isInPolicy && x.isSoldOut);
            assert(isPreferredHotelsInOrder(nonPreferredHotelsSoldOutHotels), 'The non preferred Inpolicy sold out hotels are not sorted by nearest distance.');
            let nonPreferredHotelsOOPpolicyHotels=nonPrivateRateNonPreferredHotels.filter(x => !x.policy.isInPolicy && !x.isSoldOut);
            assert(isPreferredHotelsInOrder(nonPreferredHotelsOOPpolicyHotels), 'The non preferred OOP policy non soldOut hotels are not sorted by nearest distance.');            
            let nonPreferredHotelsOOPpolicySoldOOutHotels=nonPrivateRateNonPreferredHotels.filter(x => !x.policy.isInPolicy && x.isSoldOut);
            assert(isPreferredHotelsInOrder(nonPreferredHotelsOOPpolicySoldOOutHotels), 'The non preferred OOP policy soldOut hotels are not sorted by nearest distance');    
            break;
        case "private rate":
            let privateRateInpolicyNonSoldOutHotels=sortedHotels.filter(x => x.privateRate != null && (x.policy.isInPolicy && !x.isSoldOut));
            assert(isPreferredHotelsInOrder(privateRateInpolicyNonSoldOutHotels), 'The private rate Inpolicy hotels are not sorted by nearest distance.');
            let privateRateInpolicySoldOutHotels=sortedHotels.filter(x => x.privateRate != null && (x.policy.isInPolicy && x.isSoldOut));
            assert(isPreferredHotelsInOrder(privateRateInpolicySoldOutHotels), 'The private rate Inpolicy sold out hotels are not sorted by nearest distance.');
            let privateRateOOPpolicyHotels=sortedHotels.filter(x => x.privateRate != null && (!x.policy.isInPolicy && !x.isSoldOut));
            assert(isPreferredHotelsInOrder(privateRateOOPpolicyHotels), 'The private rate OOP policy non soldOut hotels are not sorted by nearest distance.');            
            let privateRateOOPpolicySoldOOutHotels=sortedHotels.filter(x => x.privateRate != null && (!x.policy.isInPolicy && x.isSoldOut));
            assert(isPreferredHotelsInOrder(privateRateOOPpolicySoldOOutHotels), 'The private rate OOP policy soldOut hotels are not sorted by nearest distance');           
            break;
        case "most preferred":
            let mostPreferredHotelsInpolicyNonSoldOutHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'MOST_PREFERRED' && (x.policy.isInPolicy && !x.isSoldOut));
            assert(isPreferredHotelsInOrder(mostPreferredHotelsInpolicyNonSoldOutHotels), 'The  most preferred Inpolicy hotels are not sorted by nearest distance.');
            let mostPreferredHotelsSoldOutHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'MOST_PREFERRED' && (x.policy.isInPolicy && x.isSoldOut));
            assert(isPreferredHotelsInOrder(mostPreferredHotelsSoldOutHotels), 'The most preferred Inpolicy sold out hotels are not sorted by nearest distance.');
            let mostPreferredHotelsOOPpolicyHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'MOST_PREFERRED' && (!x.policy.isInPolicy && !x.isSoldOut));
            assert(isPreferredHotelsInOrder(mostPreferredHotelsOOPpolicyHotels), 'The most preferred OOP policy non soldOut hotels are not sorted by nearest distance.');            
            let mostPreferredHotelsOOPpolicySoldOOutHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'MOST_PREFERRED' && (!x.policy.isInPolicy && x.isSoldOut));
            assert(isPreferredHotelsInOrder(mostPreferredHotelsOOPpolicySoldOOutHotels), 'The most preferred OOP policy soldOut hotels are not sorted by nearest distance');           
            break;
        case "highly preferred":
            let highlyPreferredHotelsInpolicyNonSoldOutHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'HIGHLY_PREFERRED' && (x.policy.isInPolicy && !x.isSoldOut));
            assert(isPreferredHotelsInOrder(highlyPreferredHotelsInpolicyNonSoldOutHotels), 'The  highly preferred Inpolicy hotels are not sorted by nearest distance.');
            let highlyPreferredHotelsSoldOutHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'HIGHLY_PREFERRED' && (x.policy.isInPolicy && x.isSoldOut));
            assert(isPreferredHotelsInOrder(highlyPreferredHotelsSoldOutHotels), 'The highly preferred Inpolicy sold out hotels are not sorted by nearest distance.');
            let highlyPreferredHotelsOOPpolicyHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'HIGHLY_PREFERRED' && (!x.policy.isInPolicy && !x.isSoldOut));
            assert(isPreferredHotelsInOrder(highlyPreferredHotelsOOPpolicyHotels), 'The highly preferred OOP policy non soldOut hotels are not sorted by nearest distance.');            
            let highlyPreferredHotelsOOPpolicySoldOOutHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'HIGHLY_PREFERRED' && (!x.policy.isInPolicy && x.isSoldOut));
            assert(isPreferredHotelsInOrder(highlyPreferredHotelsOOPpolicySoldOOutHotels), 'The highly preferred OOP policy soldOut hotels are not sorted by nearest distance');    
            break;
        case "preferred":
            let preferredHotelsInpolicyNonSoldOutHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'HIGHLY_PREFERRED' && (x.policy.isInPolicy && !x.isSoldOut));
            assert(isPreferredHotelsInOrder(preferredHotelsInpolicyNonSoldOutHotels), 'The  preferred Inpolicy hotels are not sorted by nearest distance.');
            let preferredHotelsSoldOutHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'HIGHLY_PREFERRED' && (x.policy.isInPolicy && x.isSoldOut));
            assert(isPreferredHotelsInOrder(preferredHotelsSoldOutHotels), 'The preferred Inpolicy sold out hotels are not sorted by nearest distance.');
            let preferredHotelsOOPpolicyHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'HIGHLY_PREFERRED' && (!x.policy.isInPolicy && !x.isSoldOut));
            assert(isPreferredHotelsInOrder(preferredHotelsOOPpolicyHotels), 'The preferred OOP policy non soldOut hotels are not sorted by nearest distance.');            
            let preferredHotelsOOPpolicySoldOOutHotels=nonPrivateRatePreferredHotels.filter(x => x.preference.code == 'HIGHLY_PREFERRED' && (!x.policy.isInPolicy && x.isSoldOut));
            assert(isPreferredHotelsInOrder(preferredHotelsOOPpolicySoldOOutHotels), 'The preferred OOP policy soldOut hotels are not sorted by nearest distance');    
            break;
    }
});
