"use strict";

const commonUtils = require("../../helpers/common/common_utils");
const { createHotelSearch, getHotelsGql, getRoomDetails } = require("../../helpers/hotels/hotel_search_request_builders.js");
const { replaceRoom } = require("../../helpers/hotels/hotel_search_request_builders");
const { getHotelReviews } = require("../../helpers/hotels/hotel_search_request_builders");
const assert = require("assert");
const { setgroups } = require("process");
var scenarioStore = gauge.dataStore.scenarioStore;
step("Search for <searchType> hotels for specified nights based on table data <hotelSearchTable>", async function (searchType, hotelSearchTable) {
    let latLongDetails = commonUtils.getLatLong(hotelSearchTable.rows[0].cells[0]);
    let checkOutAfterDays = parseInt(hotelSearchTable.rows[0].cells[1]) + parseInt(hotelSearchTable.rows[0].cells[2]);
    let getHotelSearchResponse = await createHotelSearch(
        latLongDetails.latitude,
        latLongDetails.longitude,
        commonUtils.calcFutureDate(parseInt(hotelSearchTable.rows[0].cells[1])),
        commonUtils.calcFutureDate(checkOutAfterDays),
        searchType
    );
    assert(getHotelSearchResponse.data.data && !getHotelSearchResponse.data.errors, "Hotel search was not successful.");
    scenarioStore.put("hotelResultId", getHotelSearchResponse.data.data.createSearch.hotelResultId);
    let getHotelsResponse = await getHotelsGql();
    let hotelData = getHotelsResponse.data.data;
    assert(!getHotelsResponse.data.errors, "Error occurred while fetching hotels.");
    assert(hotelData, "No data was found in api response.");
    assert(hotelData.hotels, "No hotels were found in api response.");
    scenarioStore.put("searchedHotels", getHotelsResponse);
});

step("Verify the search results include hotels of hotel type <hotelType> with rooms of policy type <policyType> and cancellable policy <roomPolicyType>", async function (hotelType, policyType, roomPolicyType) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let filteredHotelsByHotelType = commonUtils.filterHotelsByHotelType(hotels, hotelType);
    assert(filteredHotelsByHotelType.length > 0, `Hotel search results does not include hotels of hotel type ${hotelType}.`);

    let filteredHotels = filteredHotelsByHotelType.filter(hotelItem =>
        commonUtils.isHotelRoomOfPolicy(hotelItem, policyType) &&
        commonUtils.isHotelRoomOfPolicyType(hotelItem, roomPolicyType)
    );
    assert(filteredHotels.length > 0, `Hotel search results does not include hotels of hotel type ${hotelType} with rooms of policy type ${policyType} and cancellable policy ${roomPolicyType}.`);
});

step("Verify DSM messages are present in the search results <tableData>", async function (tableData) {
    let hotelMessages = scenarioStore.get("searchedHotels").data.data.hotels.messages;
    let firstDSM = hotelMessages.filter(item => item.header === tableData.rows[0].cells[0] && item.text === tableData.rows[0].cells[1]);
    let secondDSM = hotelMessages.filter(item => item.header === tableData.rows[1].cells[0] && item.text === tableData.rows[1].cells[1]);
    assert(firstDSM.length > 0, "DSM Message 1 is not present in search results.");
    assert(secondDSM.length > 0, "DSM Message 2 is not present in search results.");
    assert(hotelMessages.length > 0, "DSM Message 2 is not present in search results.");
});

step("Verify search results have at least <numHotels> hotel with <numImages> image", async function (numHotels, numImages) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let hotelsWithImages = hotels.filter(hotelItem => hotelItem.photos.length > parseInt(numImages));
    scenarioStore.put("hotelsWithImages", hotelsWithImages);
    assert(hotelsWithImages.length > 0, `Hotel search results does not have at least ${numHotels} hotel(s) with images.`);
});

step("Verify search results have required image attributes for hotels", async function () {
    let hotels = scenarioStore.get("hotelsWithImages");
    let isOK = hotels.every(hotelItem => {
        return hotelItem.photos.every(x => x.size && x.url);
    });
    assert(isOK, `Hotel search results does not have required image attributes for hotels.`);
});

step("Verify the search results and check room policy status showing in-policy when average room rate is less than or equal to <averageRoomRate>", async function (averageRoomRate) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels.filter(x => x.privateRate == null);
    let allRooms = hotels.map(x => x.rooms).flat(1);
    let roomRateLessThan150 = allRooms.filter(x => x.averageRate.primary.amount <= Number(averageRoomRate));
    assert(roomRateLessThan150.length > 0, `No rooms found with average rate less than ${averageRoomRate}.`);
    let isInPolicy = roomRateLessThan150.filter(x => x.policy.isInPolicy).length == roomRateLessThan150.length;
    assert(isInPolicy, `All rooms with total cost <= ${averageRoomRate} are not in-policy.`);
});

step("Verify the search results and check room policy status showing OOP when RoomRate is greater than <perDiem>", async function (perDiem) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels.filter(x => x.privateRate == null);
    let allRooms = hotels.map(x => x.rooms).flat(1);
    let roomRateGreaterThanPerDiem = allRooms.filter(x => x.averageRate.primary.amount > Number(perDiem));
    assert(roomRateGreaterThanPerDiem.length > 0, `No rooms found with average rate greater than ${perDiem}.`);
    let isNotInPolicy = roomRateGreaterThanPerDiem.filter(x => !x.policy.isInPolicy).length == roomRateGreaterThanPerDiem.length;
    assert(isNotInPolicy, `All rooms with total cost > ${perDiem} are not out of policy.`);
});

step("Verify the search results and check room policy status showing InPolicy when RoomRate is less than or equal to <perDiem>", async function (perDiem) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels.filter(x => x.privateRate == null);
    let allRooms = hotels.map(x => x.rooms).flat(1);
    let rateLessOrEqualPerDiem = allRooms.filter(x => x.averageRate.primary.amount <= Number(perDiem));
    assert(rateLessOrEqualPerDiem.length > 0, `No rooms found with average rate less than or equal to ${perDiem}.`);
    let isInPolicy = rateLessOrEqualPerDiem.filter(x => x.policy.isInPolicy).length == rateLessOrEqualPerDiem.length;
    assert(isInPolicy, `All rooms with total cost <= ${perDiem} are not in policy.`);
});

step("Verify the search results include hotels with room source <roomType>", async function (roomType) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    var hotelData = hotels.filter(hotelItem =>
        commonUtils.isHotelRoomTypeSource(hotelItem, roomType)
    )
    assert(hotelData.length > 0, `Hotel search results does not include hotels of roomType ${roomType}.`);
});

step("Verify the search results include <roomPolicyType> rooms", async function (roomPolicyType) {
    let filteredHotels = gauge.dataStore.scenarioStore.get('filteredHotelsByHotelType').filter(hotelItem =>
        commonUtils.isHotelRoomOfPolicyType(hotelItem, roomPolicyType));
    assert(filteredHotels.length > 0, `Hotel search results does not include rooms of room policy type ${roomPolicyType}` );
});
step("Replace the old room with a new room in the itinerary", async function () {
    let replaceRoomId = scenarioStore.get("replacedHotelId");
    let oldKey = scenarioStore.get("selectedRoomKey");
    let newKey = scenarioStore.get("replacedRoomKey");
    let getReplaceRoomResponse = await replaceRoom(scenarioStore.get("checkinDate"), scenarioStore.get('checkoutDate'), scenarioStore.get('hotelResultId'),scenarioStore.get("itineraryId"), replaceRoomId, oldKey, newKey);
    assert(getReplaceRoomResponse.data.data && getReplaceRoomResponse.data.data.replaceRoom.success, `Old room with key ${oldKey} could not be replaced with new room with key ${newKey}.`);
})
step("Verify search results have private rate hotels at the top", async function () {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let privateHotelsAvailability = commonUtils.isPrivateRoomAvailable(hotels);
    assert(privateHotelsAvailability, `Hotel results do not contain private rate hotels.`);
    let privateRate = hotels.map(x => {
        if (x.privateRate == null) {
            return 0;
        }
        if (x.privateRate != null) {
            return 1;
        }
    });
    let privateRateClone = [...privateRate];
    let sortedPrivateRate = privateRateClone.sort(function (a, b) { return b - a });
    assert(JSON.stringify(privateRate) === JSON.stringify(sortedPrivateRate), `Hotel search results do not have private rate hotels at the top.`);
});
step("Verify hotel room rates are not negative", async function () {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    for (let i = 0; i < hotels.length; i++) {
        let hotelName = hotels[i].name;
        for (let j = 0; j < hotels[i].rooms.length; j++) {
            let hotelRooms = hotels[i].rooms;
            let roomName = hotelRooms[j].type;
            let rate = hotelRooms.some(x => x.averageRate.primary.amount < 0);
            assert(rate === false, `The room rate for hotel ${hotelName} with room ${roomName} is negative.`);
        }

    }

});

step("Verify search results has hotels with same day checkin", async function () {
    let getHotelsResponse = await getHotelsGql();
    let hotelData = getHotelsResponse.data.data;
    assert(!getHotelsResponse.data.errors || hotelData || hotelData.hotels, "Retrieving hotels with same day checkin failed.");
});

step("Verify room rate details", async function () {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let hotelsWithRooms = hotels.filter(x => x.rooms != null && x.rooms.length > 0);
    assert(hotelsWithRooms.length > 0, 'Hotel with rooms could not be found.');
    let randomHotelIndex = Math.floor(Math.random() * hotelsWithRooms.length);
    let selectedHotel = hotels[randomHotelIndex];
    let selectedHotelRooms = selectedHotel.rooms;
    let randomRoomIndex = Math.floor(Math.random() * selectedHotelRooms.length);
    let selectedRoom = selectedHotelRooms[randomRoomIndex];
    let response = await getRoomDetails(scenarioStore.get("checkinDate"), scenarioStore.get("checkoutDate"), scenarioStore.get("hotelResultId"), selectedHotel.id, selectedRoom.id);
    assert(response.data.data.hotelRoomDetails && response.data.data.hotelRoomDetails.rateDetails, `Room rate detail could not be found for hotel ${selectedHotel.name}.`);
    let hasValidDescription = commonUtils.hasValidRateDetails(selectedRoom.averageRate, selectedRoom.totalCost, response.data.data.hotelRoomDetails.rateDetails);
    assert(hasValidDescription, `Hotel room does not have valid room rate details for hotel ${selectedHotel.name}.`);
});

step("Verify search results have at least <numHotels> available hotel room", async function (numHotels) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let availableHotels = hotels.filter(hotelItem => !hotelItem.isSoldOut);
    assert(availableHotels.length >= numHotels, `Search result does not have at least ${numHotels} available hotel(s).`);
    let availableHotelRoom = availableHotels.filter(hotelItem => hotelItem.rooms.some(roomItem => roomItem.averageRate != null));
    assert(availableHotelRoom.length >= numHotels, `Hotel search result does not have at least ${numHotels} available hotel room(s).`);
});

step("Verify search result includes <hotelType> hotels", async function (hotelType) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let preferredHotels = commonUtils.filterHotelsByHotelType(hotels, hotelType);
    assert(preferredHotels.length > 0, `Hotel search results does not include ${hotelType} hotels.`);
    scenarioStore.put("filteredHotelsByHotelType", preferredHotels);

});

step("Verify the search results include rooms of policy type <policyType>", async function (policyType) {
    let filteredHotels = gauge.dataStore.scenarioStore.get('filteredHotelsByHotelType');
    var hotelsData = filteredHotels.filter(hotelItem =>
        commonUtils.isHotelRoomOfPolicy(hotelItem, policyType)
    )
    assert(hotelsData.length > 0, `Hotel search results does not include rooms of policy type ${policyType}`);
    scenarioStore.put("policyFilteredHotels", hotelsData);
});

step("Verify search result includes <roomPolicyType> rooms for room type <roomTypeSource>", async function (roomPolicyType, roomTypeSource) {
    let hotels = scenarioStore.get("policyFilteredHotels");
    var hotelData = hotels.filter(hotelItem =>
        commonUtils.isHotelRoomTypeSource(hotelItem, roomTypeSource)
    )
    let filteredHotels = gauge.dataStore.scenarioStore.get('filteredHotelsByHotelType');
    let hotelsData = filteredHotels.filter(hotelItem =>
        commonUtils.isHotelRoomOfPolicyType(hotelItem, roomPolicyType)
    )
    assert(hotelData.length > 0 || hotelsData.length > 0, `Hotel search results does not include ${roomPolicyType} rooms for room type ${roomTypeSource}`);
});

step("Verify hotel reviews are available", async function () {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let selectedHotel = hotels[0];
    let getHotelReviewsResponse = await getHotelReviews(selectedHotel.id);
    let hotelData = getHotelReviewsResponse.data.data.hotelReviews.reviews;
    assert(hotelData.length > 0, "Hotel reviews were not found");
});

step("Verify search results includes hotels of type <hotelType>", async function (hotelType) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let preferred = commonUtils.filterHotelsByHotelType(hotels, hotelType);
    assert (preferred = true || false, `Hotel search results does not include hotels of type ${hotelType}.`);
})

step("Verify search results include <rateType> rate hotels", async function (rateType) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let privateRateHotels = commonUtils.filterHotelsByRateType(hotels,rateType)
    assert(privateRateHotels !== null, `Hotel search results does not include ${rateType} rate hotels.`)
    
})

step("Verify all rooms in search results have room source", async function () {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    for(let i=0; i < hotels.length; i++)
    {
        for (let j = 0; j < hotels[i].rooms.length; j++) {
            let hotelRooms = hotels[i].rooms;
            let sourceType = hotelRooms[j].source;
            assert(sourceType == null || "booking.com" || "Tripsource", `Hotel search result does have valid room source.`);
        }
    }
})
step("Verify the search results does not include hotels with room source <roomSource>", async function (roomSource) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let allRooms = hotels.map(x => x.rooms).flat(1)    
    let source = commonUtils.filterHotelsByRoomSource(allRooms, roomSource);
    assert(source.length !== allRooms.length, `Hotel search results does not include hotels with room source ${roomSource}.`);

});

step("Verify the search results have hotel rooms from <roomSource> source", async function (roomSource){
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let allRooms = hotels.map(x => x.rooms).flat(1)    
    let source = commonUtils.filterHotelsByRoomSource(allRooms, roomSource);
    assert(source.length == allRooms.length, `Hotel search results does not include hotels with room source ${roomSource}.`);
})
step("Verify search results includes <hotelType> hotels", async function (hotelType) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let hotelTypePreference = commonUtils.filterHotelsByHotelType(hotels, hotelType);
    assert(hotelTypePreference.length == hotels.length, `Hotel search results does not include ${hotelType} hotels.`);

});

step("Verify search results includes <rateType> rate hotels", async function (rateType) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let hotelRateType = commonUtils.filterHotelsByRateType(hotels, rateType);
    assert(hotelRateType.length == hotels.length, `Hotel search results does not include ${rateType} rate hotels.`);
});

step("Verify <rateType> rate hotels are <policyType>", async function (rateType, policyType) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let isRateTypeInPolicy = commonUtils.hotelsOfPolicyForRateType(hotels, rateType, policyType);
    assert(isRateTypeInPolicy.length == hotels.length, `The ${rateType} rate hotels are not ${policyType}.`);
});

step("Verify search results does not include <rateType> rate hotels", async function (rateType) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let hotelRateType = commonUtils.filterHotelsByRateType(hotels, rateType);
    assert(hotelRateType.length == 0, `Hotel search results includes ${rateType} rate hotels.`);
    
	
});

step("Verify the search results have room rate less than or equal to the max rate of <roomRate>", async function (roomRate) {
    let hotels = scenarioStore.get("searchedHotels").data.data.hotels.hotels;
    let allRooms = hotels.map(x => x.rooms).flat(1);
    let roomRateLessThan350 = allRooms.filter(x => x.averageRate.primary.amount <= Number(roomRate));
    assert(roomRateLessThan350.length > 0, `Hotel rooms with room rate less than or equal to max rate of ${roomRate} not found.`);
});
