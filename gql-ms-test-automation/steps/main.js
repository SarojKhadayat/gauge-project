let xray = require("../helpers/XrayApi/xray_utils");
var ScenarioErrors = [];

afterScenario(async (context) => {
    var tags = context.currentScenario.tags.filter(arrayItem => arrayItem.includes(process.env.JIRA_TEST_PROJ_ID));
    if (tags.length && process.env.USE_XRAY.toLowerCase() == 'true') {
        await xray.xrayTestCaseUpdates(tags, context, ScenarioErrors);
    }
}, { tags: [] });

afterStep(async (context) => {
    if (context.currentStep.isFailed) {
        var Scenario = {};
        Scenario.ScenarioName = context.currentScenario.name;
        Scenario.StepText = context.currentStep.step.actualStepText;
        Scenario.StepError = context.currentStep.errorMessage;
        Scenario.SpecFile = context.currentSpec.fileName;
        Scenario.StepStackTrace = context.currentStep.stackTrace;
        ScenarioErrors.push(Scenario)
    };
}, { tags: [] });

beforeScenario(async (context) => {
    var tags = context.currentScenario.tags.filter(arrayItem => arrayItem.includes(process.env.JIRA_TEST_PROJ_ID));

    if (tags.length && process.env.USE_XRAY.toLowerCase() == 'true') {
        var tags = context.currentScenario.tags.filter(arrayItem => arrayItem.includes(process.env.JIRA_TEST_PROJ_ID));
        try {
            typeof process.env.JIRA_TEST_PLAN_ID == 'undefined' | !process.env.JIRA_TEST_PLAN_ID.includes(process.env.JIRA_TEST_PROJ_ID);
        } catch (error) {
            throw `USE_XRAY is set to 'true' but No test plan Id was passed in - please add to the CI or 'default.properties' and try again`;
        }
        await xray.checkTestExecutionId();
    }

}, { tags: [] });
