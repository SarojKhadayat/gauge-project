step("Retrieve environment data", async function() {
    switch(process.env.environment) {
        case 'qa10':
            gauge.dataStore.suiteStore.put('baseurl', 'http://scqa10lappca01.scqa.rden.us:8011/');
            gauge.dataStore.suiteStore.put('gqlurl', 'https://fijiphx.rcqa10.com/');
            gauge.dataStore.suiteStore.put('tld', 'rcqa10.com');
            gauge.dataStore.suiteStore.put('gazooenv','rcqa10')
            break;
        case 'qa15':
            gauge.dataStore.suiteStore.put('baseurl', 'http://scqa15lappca01.scqa.rden.us:8011/');
            gauge.dataStore.suiteStore.put('gqlurl', 'https://phxhss.rcqa15.com/');
            gauge.dataStore.suiteStore.put('tld', 'rcqa15.com');
            gauge.dataStore.suiteStore.put('gazooenv','rcqa15')
            break;
        case 'sustain':
            gauge.dataStore.suiteStore.put('baseurl', 'https://xxxx.rcsustain.com/');
            gauge.dataStore.suiteStore.put('gqlurl', 'https://platform.rcsustain.com/');
            gauge.dataStore.suiteStore.put('tld', 'rcsustain.com')
            break;
    }    
});