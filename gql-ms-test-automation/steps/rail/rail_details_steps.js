"use strict";

const commonUtils = require("../../helpers/common/common_utils");
const { getRailJourneyPreferences, updateJourneyPreferences} = require("../../helpers/rail/rail_search_request_builders");

const assert = require("assert");
var scenarioStore = gauge.dataStore.scenarioStore;
var railLeg;
var journeyType;
const preferredCurrency = "GBP";

step("Select <legType> <railType> train leg", async function (legType, railType) {
    const railResult = scenarioStore.get("railResult");
    journeyType = (railType === "departing") ? "departing" : "arriving";
    scenarioStore.put("journeyType", journeyType);
    var allRailLegs = (journeyType == "departing") ? railResult.outwardJourneys : railResult.inwardJourneys;

    let getRandomLegNo = (legType == "first") ? 0 : Math.floor(Math.random() * allRailLegs.length );
    railLeg = allRailLegs[getRandomLegNo]
    assert((railLeg.legId != ""), "The rail leg does not have an id.");
});

step("Verify origin and destination stations", async function () {
    let originStation = (journeyType == "departing") ? scenarioStore.get("railSearchOriginStation") : scenarioStore.get("railSearchDestStation");
    assert((railLeg.origin != "" && railLeg.origin === originStation), `The rail leg does not have a origin station or it does not match the original origin station ${originStation}.`);

    let destStation = (journeyType == "departing") ? scenarioStore.get("railSearchDestStation") : scenarioStore.get("railSearchOriginStation");
    assert((railLeg.destination != "" && railLeg.destination === destStation), `The rail leg does not have a destination station or it does not match the original destination station ${destStation}.`);
});

step("Verify carrier name, logo and train number", async function () {

    assert((railLeg.segments[0].carrier.name !== ""), "The rail leg segment does not have a carrier name.");
    assert((railLeg.segments[0].carrier.logoImageUrl), "The rail leg segment does not have a carrier logo url.");
    assert((railLeg.segments[0].trainNumber > 0), "The rail leg segment does not have a valid train number.");
});

step("Verify departure and arrival dates", async function () {

    let departureDate = (journeyType == "departing") ? scenarioStore.get("railSearchDepartureDate") : scenarioStore.get("railSearchArrivalDate");
    console.log(JSON.stringify(railLeg))
    assert((railLeg.departureDate != "" && railLeg.departureDate.includes(departureDate)), `The rail leg does not have a valid departure date or it does not match the original departure date from railLeg ${railLeg.departureDate} and session ${departureDate} for the ${journeyType} train.`);
    assert((railLeg.arrivalDate != "" && railLeg.arrivalDate.includes(departureDate)), "The rail leg does not have a valid arrival date.");
});


step("Verify travel time", async function () {

    assert((railLeg.railTime && railLeg.railTime.hours >= 0 && railLeg.railTime.minutes >= 0), "The rail leg does not have a travel time.");
});

step("Verify rate amount", async function () {

    assert((railLeg.rate.primary.amount > 0), "The rail leg does not have a valid primary rate.");
    assert((railLeg.rate.secondary.amount > 0), "The rail leg does not have a valid secondary rate.");
});

step("Verify fare amount", async function () {

    assert((railLeg.fares[0].total.primary.amount > 0), "The rail leg does not have a valid primary total fare.");
    assert((railLeg.fares[0].total.secondary.amount > 0), "The rail leg does not have a valid secondary total fare.");
});

step("Verify platform number", async function() {

    assert((railLeg.platform !== null), "The rail leg does not have a valid platform number") 
});

step("Verify fare amount displayed on user preferred currency", async function() {

    assert((railLeg.fares[0].total.primary.currency === preferredCurrency), "The rail leg fare does not displayed based on user preferred currency.");
});

step("Verify fare tier information", async function() {
    var fareTier =  scenarioStore.get("railTierInformation");
    assert((fareTier.id !== null), "The fare conditions for the available fares does not have a valid fare tier id.");
    assert((fareTier.name !== null), "The fare conditions for the available fares does not have a valid fare tier name.");
});

step("Verify localized fare conditions for standard fare", async function() {
    var railFareConditions = scenarioStore.get("railFareConditions");
    var localizedFareConditions = railFareConditions.localizedFareConditions[0];
    assert((localizedFareConditions.fareTypeDescription !== null), "The fare conditions for the standard fares does not have a valid fare type description.");
});


step("Verify rail card code", async function() {
    let railCardsResut = scenarioStore.get("railCardsResult");
    let getRandomIndex = commonUtils.calcRandomInt(railCardsResut.railCards.cards.length-1);
    let getRandonRailCard = railCardsResut.railCards.cards[getRandomIndex];
    scenarioStore.put("randonRailCardResult", getRandonRailCard);
    assert(railCardsResut.railCards.cards.length > 0,  "No rail cards were returned.")
    assert(getRandonRailCard.code != null, "The rail card does not have code")
});

step("Verify rail card name", async function() {
	assert(scenarioStore.get("randonRailCardResult").name != null, "The rail card does not have name")
});

step("Verify rail card groupname", async function() {
    assert(scenarioStore.get("randonRailCardResult").groupName != null, "The rail card does not group name")
});

step("Verify rail card vendorname displayed on <expectedVendorName>", async function(expectedVendorName) {
    assert(scenarioStore.get("randonRailCardResult").vendorName === expectedVendorName, "Expected vendor name not displayed on rail card")
});

step("Verify rail card vendorcode displayed on <expectedVendorCode>", async function(expectedVendorCode) {
    assert(scenarioStore.get("randonRailCardResult").vendorCode === expectedVendorCode, "Expected vendor code not displayed on rail card")
});

step("Verify <groupType> list order", async function(groupType) {
    let railCardsBasedOnGroupType =  scenarioStore.get("railCardsResult").railCards.cards.filter(x => x.groupName == groupType);
    let sortedRailCardsBasedOnGroupType = [...railCardsBasedOnGroupType];
    sortedRailCardsBasedOnGroupType.sort((a, b) => a - b);
    assert(JSON.stringify(railCardsBasedOnGroupType) === JSON.stringify(sortedRailCardsBasedOnGroupType), "The rail cards not displayed on order")
});

step("Verify the stop information of the rail stops for all the segments", async function () {
    console.log(`Splunk Logging Error ${JSON.stringify(scenarioStore.get("railResult"))}`)
    let railStopsNo = railLeg.railStops
    assert(railStopsNo > 0,  "No rail stops were returned for the selected rail leg.")
    let totalRailStopsForEachSegments = 0;
    for (var seg of railLeg.segments) {
        totalRailStopsForEachSegments += seg.stops.length;
        for (var i = 0; i < seg.stops.length; i++) {
            assert(seg.stops[i].stationName != null, "The rail stop doesnt have the station's name");
        }
    }
    assert(totalRailStopsForEachSegments ===  railStopsNo,  "Rail Stops total is not matching the individual segments rail stops count.")
});

step("Verify the duration information of the rail stops for all the segments", async function () {
    console.log(`Splunk Logging Error ${JSON.stringify(scenarioStore.get("railResult"))}`)
    let totalRailStopsForEachSegments = 0;
    for (var seg of railLeg.segments) {
        totalRailStopsForEachSegments += seg.stops.length;
        for (var i = 0; i < seg.stops.length; i++) {
            assert(seg.stops[i].departureTime != null, "The rail stop doesnt have the station's departure time")
            if(i > 0){
                assert(seg.stops[i].departureTime > seg.stops[i-1].departureTime, `The departure time for the current stop ${seg.stops[i].departureTime} is not greater than its previous stop's departure time ${seg.stops[i-1].departureTime}`,)
            }
        }
    }
});       

step("Get rail journey preferences", async function() {
    let railJourneyPreferencesResponse = await getRailJourneyPreferences(scenarioStore.get("tripId"));
    assert((railJourneyPreferencesResponse.data.data && !railJourneyPreferencesResponse.data.errors), "Get rail journey preferences call failed with errors.");
    assert((railJourneyPreferencesResponse.data.data.railJourneyPreferences.deliveryOptions[0].code !== null), "Get rail journey preferences does not have a valid delivery options.");
    scenarioStore.put("railJourneyPreferences", railJourneyPreferencesResponse.data.data.railJourneyPreferences);
});

step("Update journey preferences with delivery option", async function() {
    let updateJourneyPreferencesResponse = await updateJourneyPreferences(scenarioStore.get("tripId"), scenarioStore.get("railJourneyPreferences").deliveryOptions[0].code);
    assert((updateJourneyPreferencesResponse.data.data && !updateJourneyPreferencesResponse.data.errors), "Update journey preferences call failed with errors.");
    assert((updateJourneyPreferencesResponse.data.data.updateRailJourneyPreferences.success), "Updating journey preferences with delivery option failed.");
});