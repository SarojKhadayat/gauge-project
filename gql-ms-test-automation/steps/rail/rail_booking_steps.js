"use strict";

const {addRail} = require("../../helpers/rail/rail_search_request_builders");

const assert = require("assert");
var scenarioStore = gauge.dataStore.scenarioStore;

step("Select <fareType> fare leg", async function(fareType) {
    const railResult = scenarioStore.get("railResult");
    var allRailLegs = (scenarioStore.get("journeyType") === "departing") ? railResult.outwardJourneys : railResult.inwardJourneys;
    let getRandomLegNo = Math.floor(Math.random() * allRailLegs.length )
    let tripId = allRailLegs[getRandomLegNo].fares.filter(x => x.segments[0].displayServiceClass === fareType)[0].tripId;
    assert((tripId !== null), `The fare trip Id is not present for the chosen fareType - ${fareType}.`);
    console.log(`${fareType} Fare TripID: ${tripId}`);
    scenarioStore.put("tripId", tripId);
});

step("Add rail trip to the itinerary", async function() {
    let getAddRailResponse = await addRail(scenarioStore.get("tripId"), scenarioStore.get("itineraryId"));
    assert((getAddRailResponse.data.data || !getAddRailResponse.data.errors), "Get Add Rail call failed with errors."); 
    assert((getAddRailResponse.data.data.addRail.success), "Get Add Rail failed and returned no sucess response");
});