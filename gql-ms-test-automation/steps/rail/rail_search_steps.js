"use strict";

const commonUtils = require("../../helpers/common/common_utils");
const { createRailSearch, getRail, getRailCards, getItinerary} = require("../../helpers/rail/rail_search_request_builders");
const assert = require("assert");
var scenarioStore = gauge.dataStore.scenarioStore;


step("Search for rail with origin and destination stations for specified date/time based on table data <railSearchTable>", async function (railSearchTable) {
    let originStation = railSearchTable.rows[0].cells[0];
    let destStation = railSearchTable.rows[0].cells[1];
    let daysUntilDept = railSearchTable.rows[0].cells[2];
    let noOfPassengers = parseInt(railSearchTable.rows[0].cells[3]);
    let departureDate = commonUtils.calcFutureDate(parseInt(daysUntilDept));

    let getRailSearchResponse = await createRailSearch(
        originStation, 
        destStation,
        departureDate,
        null,
        noOfPassengers
    );
    await assertCreateRailSearchAndUpdateSession(getRailSearchResponse, originStation, destStation, departureDate)
});


step("Search for return rail with origin and destination stations for onward and outward journey with specified date/time based on table data <railSearchTable>", async function (railSearchTable) {
    let originStation = railSearchTable.rows[0].cells[0];
    let destStation = railSearchTable.rows[0].cells[1];
    let daysUntilDept = railSearchTable.rows[0].cells[2];
    let daysUntilArrival = railSearchTable.rows[0].cells[3];
    let noOfPassengers = parseInt(railSearchTable.rows[0].cells[4]);
    let departureDate = commonUtils.calcFutureDate(parseInt(daysUntilDept));
    let arrivalDate = commonUtils.calcFutureDate(parseInt(daysUntilArrival));

    let getRailSearchResponse = await createRailSearch(
        originStation,
        destStation,
        departureDate,
        arrivalDate,
        noOfPassengers
    );
    await assertCreateRailSearchAndUpdateSession(getRailSearchResponse, originStation, destStation, departureDate)
    scenarioStore.put("railSearchArrivalDate", arrivalDate);
});


async function assertCreateRailSearchAndUpdateSession(getRailSearchResponse, originStation, destStation, departureDate) {
    assert((getRailSearchResponse.data.data || !getRailSearchResponse.data.errors), "Rail search failed with errors.");
    assert((getRailSearchResponse.data.data.createRailSearch.id), "Rail search failed and returned no search id.");
    scenarioStore.put("railSearchId", getRailSearchResponse.data.data.createRailSearch.id);
    scenarioStore.put("railSearchOriginStation", originStation);
    scenarioStore.put("railSearchDestStation", destStation);
    scenarioStore.put("railSearchDepartureDate", departureDate);
}

step("Get list of trains", async function () {
    let railSearchId = scenarioStore.get("railSearchId");
    let originStation = scenarioStore.get("railSearchOriginStation");
    let destStation = scenarioStore.get("railSearchDestStation");
    let getRailResponse = await getRail(
        railSearchId,
        originStation,
        destStation
    );
    assert((getRailResponse.data.data || !getRailResponse.data.errors), "Get Rail call failed with errors.");
    assert((getRailResponse.data.data.rail), "Get Rail call failed and returned no results for departing trains.");
    scenarioStore.put("railResult", getRailResponse.data.data.rail);
});


step("Verify at least <numTrains> <journeyType> trains legs are returned", async function (numTrains, journeyType) {

    let railResult = scenarioStore.get("railResult");
    let inwardRailLegs = 0
    assert((railResult.outwardJourneys.length > 0), "No outward rail legs were returned.");

    if(journeyType === "departing and arriving"){
        numTrains = numTrains * 2
        inwardRailLegs = railResult.inwardJourneys.length
        assert((railResult.inwardJourneys.length > 0), "No inward rail legs were returned.");
    }

    assert((railResult.totalCount >= numTrains), `${railResult.totalCount} total trains were returned. Expected at least ${numTrains}.`);
    assert((railResult.outwardJourneys.length + inwardRailLegs === railResult.totalCount), `${railResult.totalCount} ${journeyType} trains were returned but only ${railResult.outwardJourneys.length + inwardRailLegs} had rail legs.`);
    });

step("Get rail cards for my rail journey", async function() {
    let getRailCardsResponse = await getRailCards();
    scenarioStore.put("railCardsResult", getRailCardsResponse.data.data);
});

step("Get stand alone fare conditions", async function () {
    let getItineraryResponse =  scenarioStore.get("itineraryResponse");

    assert((getItineraryResponse.segments[0]), "Retrieving segments in the itinerary failed.");
    assert((getItineraryResponse.segments[0].trainLeg), "Retrieving train leg for segments in the itinerary failed.");
    assert((getItineraryResponse.segments[0].trainLeg.tier), "Retrieving tier for train leg in the itinerary failed.");
    assert((getItineraryResponse.segments[0].trainLeg.fareConditions), "Retrieving fare conditions for train leg in the itinerary failed.");
    scenarioStore.put("railFareConditions", getItineraryResponse.segments[0].trainLeg.fareConditions);
    assert((getItineraryResponse.segments[0].trainLeg.tier), "Retrieving tier for train leg in the itinerary failed.");
    scenarioStore.put("railTierInformation", getItineraryResponse.segments[0].trainLeg.tier);
});


step("Get rail itinerary", async function() {
    let itineraryId = scenarioStore.get("itineraryId");

    let getItineraryResponse = await getItinerary(
        itineraryId
    );

    assert((getItineraryResponse.data.data && !getItineraryResponse.data.errors), "Get itinerary failed with errors.");
    assert((getItineraryResponse.data.data.itinerary), "Retrieving itinerary information failed.");
    scenarioStore.put("itineraryResponse", getItineraryResponse.data.data.itinerary);

});

