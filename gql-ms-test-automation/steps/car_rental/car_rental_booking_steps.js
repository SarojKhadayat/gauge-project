"use strict";

const commonUtils = require("../../helpers/common/common_utils");
const { createCarRentalSearch } = require("../../helpers/car_rental/car_rental_search_request_builders");
const assert = require("assert");
const { stringify } = require("jsonpath");
const { CANCELLED } = require("dns");
const { verify } = require("crypto");
var scenarioStore = gauge.dataStore.scenarioStore;
const defaultSearchTime = "T11:00:00.000"; 

step("Search for car rentals with combination of pickup/drop off airport/non-airport locations based on input data <carRentalSearchTable>", async function (carRentalSearchTable) {
    let pickUpAirport = "";
    let dropOffAirport = "";
    let pickUpLocationId = "";
    let dropOffLocationId = "";

    // when searching by pickup from airport code, pass this airport code value and no need to pass LocationId in pickUpLocationId
    if(carRentalSearchTable.rows[0].cells[0]){
        pickUpAirport = carRentalSearchTable.rows[0].cells[0]
    }else{
        if(carRentalSearchTable.rows[0].cells[2]){
            pickUpLocationId = commonUtils.getLatLong(carRentalSearchTable.rows[0].cells[2]).locationId
        }else{
            assert((carRentalSearchTable.rows[0].cells[0] || carRentalSearchTable.rows[0].cells[2]), "The pick up airport and pick up location are missing.");
        }
    }
    // when searching by dropoff from airport code, pass this airport code value and no need to pass LocationId in dropOffLocationId
    if(carRentalSearchTable.rows[0].cells[1]){
        dropOffAirport = carRentalSearchTable.rows[0].cells[1]
    }else{
        if(carRentalSearchTable.rows[0].cells[3]){
            dropOffLocationId = commonUtils.getLatLong(carRentalSearchTable.rows[0].cells[3]).locationId
        }else{
            assert((carRentalSearchTable.rows[0].cells[1] || carRentalSearchTable.rows[0].cells[3]), "The drop off airport and drop off location are missing.");
        }
    }

    let checkOutAfterDays = parseInt(carRentalSearchTable.rows[0].cells[4]) + parseInt(carRentalSearchTable.rows[0].cells[5]);
    let pickUpDateTime = commonUtils.calcFutureDate(parseInt(carRentalSearchTable.rows[0].cells[4]))+defaultSearchTime;
    let dropOffDateTime = commonUtils.calcFutureDate(checkOutAfterDays)+defaultSearchTime;

    // For some tests additinal test data modifications required before call to search based on rule described in "Special test data notes"
    if (carRentalSearchTable.rows[0].cells[6]){
        switch(carRentalSearchTable.rows[0].cells[6]){
            case "Pickup modified to next Monday if it's Sat or Sun":
                pickUpDateTime = modifySatOrSunToMon(pickUpDateTime, parseInt(carRentalSearchTable.rows[0].cells[4]));
                break;
            case "Dropoff modified to next Monday if it's Sat or Sun":
                dropOffDateTime = modifySatOrSunToMon(dropOffDateTime, checkOutAfterDays);
                break;
            case "Pickup and drop-off dates modified to next Monday if it's Sat or Sun":                
                // If pickup day is Saturday add 2 days or 1 day if Sunday to move pickup day to Monday    
                pickUpDateTime = modifySatOrSunToMon(pickUpDateTime, parseInt(carRentalSearchTable.rows[0].cells[4]));           
                // If drop-off day is Saturday add 2 days or 1 day if Sunday to move drop-off day to Monday    
                dropOffDateTime = modifySatOrSunToMon(dropOffDateTime, checkOutAfterDays);
                break;
        default:
        }
    }

    let getCarRentalSearchResponse = await createCarRentalSearch(
        pickUpDateTime, 
        dropOffDateTime, 
        pickUpAirport, 
        dropOffAirport, 
        pickUpLocationId,
        dropOffLocationId
    );

    assert((getCarRentalSearchResponse.data.data || !getCarRentalSearchResponse.data.errors), "Car Rental search failed with errors");
    scenarioStore.put("carRentalInputNumDays", carRentalSearchTable.rows[0].cells[5]);
    scenarioStore.put("carRentalSearchId", getCarRentalSearchResponse.data.data.createCarRentalSearch.searchId);
    scenarioStore.put("carRentalSearchCars", getCarRentalSearchResponse.data.data.createCarRentalSearch.car);
});
 
function modifySatOrSunToMon(dateTimeIn,daysToPickUp){
    // If day is Saturday add 2 days or 1 day if Sunday to move day to Monday    
    if (new Date(dateTimeIn).getDay() == 6){
        // override from Saturday to Monday
        return commonUtils.calcFutureDate((daysToPickUp+2))+defaultSearchTime;
    } else if(new Date(dateTimeIn).getDay() == 0) {
        // override from Sunday to Monday
        return commonUtils.calcFutureDate((daysToPickUp+1))+defaultSearchTime;
    } else return dateTimeIn
}

step("Checkout a car rental", async function () {
 // TODO
 // assert( Reservation ID is received );
});

step("Verify reservation was successful", async function () {
    // TODO
    // assert Reservation ID status is <CONFIRMED>
});

step("Verify reservation with id <reservationID> has status of <expectedReservationStatus>", async function (expectedReservationStatus) {
    verifyReservationIDStatusIs(reservationID, expectedReservationStatus);
    // TODO call to View flow with Reservation ID to assure that status is the same as expected
    // For now expected status cases are - CONFIRMED OR CANCELLED
});
