const { createCartResponse, addToCart, getCart, removeFromCart} = require("../../helpers/common/common_request_builders");
const assert = require("assert");
var scenarioStore = gauge.dataStore.scenarioStore;

step("Select first car from search results and add to cart", async function () {
    let createCartResponseVar = await createCartResponse()
    assert((!('message' in createCartResponseVar.data.data)), "The cart could not be created.");
    scenarioStore.put("cartId", createCartResponseVar.data.data.createCart.cartId)

    let cars = scenarioStore.get("carRentalSearchCars");
    let cartId = scenarioStore.get("cartId");
    let searchId = scenarioStore.get("carRentalSearchId");

    gauge.dataStore.scenarioStore.put("inventoryId", cars[0].key);

    let addToCartResponse = await addToCart(cartId, "CAR_RENTAL", "MODIFY", cars[0].key, searchId)
    assert(!('message' in addToCartResponse.data.data), `The selected car ${cars[0].key} could not be added to the cart.`);
    scenarioStore.put("intentId", addToCartResponse.data.data.addToCart.intentId)
});

step("Verify that car added to cart", async function () {
    let cartId = scenarioStore.get("cartId");
    let intentId = scenarioStore.get("intentId");
    let getCartResponse = await getCart(cartId)
    assert(!('message' in getCartResponse.data.data), `The cart with intent ${cartId} could not be taken`);
    assert(getCartResponse.data.data.cart.intents.length > 0, `The cart with intent ${cartId} is empty`);
    let intentFromCart = getCartResponse.data.data.cart.intents[0].id;
    assert(intentFromCart == intentId, `The car with intent ${intentId} not found in cart with id ${cartId} `);
});

step("Remove selected car from cart", async function () {
    let cartId = scenarioStore.get("cartId");
    let intentId = scenarioStore.get("intentId");
    let removeFromCartResponse = await removeFromCart(cartId, intentId)
    assert((removeFromCartResponse.data.data), `Something went wrong during removing car ${intentId} from cart with id ${cartId}`);
    scenarioStore.put("removeMessage", removeFromCartResponse.data.data.removeFromCart.message)
});

step("Verify that car was removed from the cart", async function () {
    let cartId = scenarioStore.get("cartId");
    let intentId = scenarioStore.get("intentId");
    let message = scenarioStore.get("removeMessage");
    assert(message == "removed intent from cart successfully", `The car with intent ${intentId} could not be removed from cart id ${cartId}}`);
});

step("Verify cart is empty", async function () {
    let cartId = scenarioStore.get("cartId");
    let getCartResponse = await getCart(cartId)
    assert((getCartResponse.data.data || !getCartResponse.data.errors || !('message' in getCartResponse.data.data)), `The cart with id ${cartId} could not be taken}`);
    //assert(getCartResponse.data.data.cart.intents.length == 0, "Cart mustn't contains eny items");
});

step("Get car additional info and validate total price calculation, price per day, extra hour charge", async function () {
// TODO implement this method
});

step("Get car additional info and validate that car same as picked from search", async function () {
// TODO implement this method
});

step("Verify trip was updated with latest added car", async function () {
// TODO implement this method
});

step("Select another car from the search results", async function() {
       // TODO implement this method
});

