"use strict";

const commonUtils = require("../../helpers/common/common_utils");
const { createCarRentalSearch } = require("../../helpers/car_rental/car_rental_search_request_builders");
const assert = require("assert");
const { stringify } = require("jsonpath");
var scenarioStore = gauge.dataStore.scenarioStore;

step("Search for car rentals for same pickup/drop off <locationType> location based on input data <carRentalSearchTable>", async function (locationType, carRentalSearchTable) {
    // when searching by airport code, pass in the airport code as is. No need to pass in LocationIds
    // when searching by city or location, pass in the LocationId (translated from the lat/long coordinates)

    let pickUpAirport;
    let dropOffAirport;
    let pickUpLocationId;
    let dropOffLocationId;
    if(locationType === "airport"){
        pickUpAirport = carRentalSearchTable.rows[0].cells[0];
        dropOffAirport = carRentalSearchTable.rows[0].cells[1];
        pickUpLocationId = "";
        dropOffLocationId = "";
    }
    if(locationType === "non airport"){
        pickUpAirport = "";
        dropOffAirport = "";
        pickUpLocationId = commonUtils.getLatLong(carRentalSearchTable.rows[0].cells[0]).locationId || "0";
        dropOffLocationId = commonUtils.getLatLong(carRentalSearchTable.rows[0].cells[1]).locationId || "0";
    }
    let checkOutAfterDays = parseInt(carRentalSearchTable.rows[0].cells[2]) + parseInt(carRentalSearchTable.rows[0].cells[3]);
    let pickUpDateTime = commonUtils.calcFutureDate(parseInt(carRentalSearchTable.rows[0].cells[2]))+'T11:00:00.000';
    let dropOffDateTime = commonUtils.calcFutureDate(checkOutAfterDays)+'T11:00:00.000';
    let getCarRentalSearchResponse = await createCarRentalSearch(
        pickUpDateTime, 
        dropOffDateTime, 
        pickUpAirport, 
        dropOffAirport, 
        pickUpLocationId,
        dropOffLocationId
    );
    assert((getCarRentalSearchResponse.data.data || !getCarRentalSearchResponse.data.errors), "Car Rental search failed with errors");
    scenarioStore.put("carRentalInputNumDays", carRentalSearchTable.rows[0].cells[3]);
    scenarioStore.put("carRentalSearchId", getCarRentalSearchResponse.data.data.createCarRentalSearch.searchId);
    scenarioStore.put("carRentalSearchCars", getCarRentalSearchResponse.data.data.createCarRentalSearch.car);
});

step("Verify at least <numResults> search results are returned by default", async function (numResults) {
    let cars = scenarioStore.get("carRentalSearchCars");
    assert(cars.length >= numResults, `Expected: ${numResults} Actual: ${cars.length}`);
});

step("Verify at least <numResults> car(s) is returned", async function (numResults) {
    let cars = scenarioStore.get("carRentalSearchCars");
    assert(cars.length >= numResults, `Expected: ${numResults} Actual: ${cars.length}`);
});

step("Verify the search results includes car rental of brand <carBrand>", async function (carBrand) {    
    let cars = scenarioStore.get("carRentalSearchCars");
    carBrand = carBrand.split(" |OR| ") // convert to array to verify with each expected vendor
    const brandFound = cars.reduce(function(previousValue, currentValue) {  
        for (let i = 0; i < carBrand.length; i++) {
            if(currentValue.vendor.name === carBrand[i]) return true
        }
    });    
    assert(brandFound, `Search results did not include any car rentals of brand '${carBrand}'`);
});

step("Verify search result includes details for the car rental", async function () {
    for (const car of scenarioStore.get("carRentalSearchCars")) {
        await verifyCar(car);
    }
});

step("Verify total price calculation, price per day, extra hour charge", async function () {
    for (const car of scenarioStore.get("carRentalSearchCars")) {
        await verifyRate(car);
    }
});

async function verifyCar(car) {
    assert(car.details !== undefined, "Car Rental details are missing");
    assert(car.details.carImageUrl !== undefined || car.details.imageUrl !== undefined, "Image url is missing");
    assert(car.details.passenger !== undefined, "Passenger is missing");
    assert(car.details.luggage !== undefined, "Bags/Luggage is missing");
    assert(car.details.transmission !== undefined, "Transmission Type is missing");
    assert(car.details.isAirConditioned !== undefined, "Air Conditioner is missing");
    assert(car.details.contractName !== undefined, "Contract Name is missing");
    // assert(car.details.doors !== undefined, "Doors is missing");
    assert(car.details.name !== undefined, "Car Make Model is missing");
    assert(car.safetyCheck !== undefined, "Safety Check is missing");
    await verifyLocation(car.pickUp)
    await verifyLocation(car.dropOff)
    await verifyDateTime(car.pickUp)
    await verifyDateTime(car.dropOff)
    await verifyVendor(car.vendor)
    assert(car.unlimitedMileage !== undefined, "Mileage is missing");
    // assert(car.suppliedBy !== undefined, "Supplied By is missing");
}

async function verifyLocation(location){
    assert(location.locationId !== undefined, "Location id is missing");
    assert(location.isAirportLocation !== undefined, "Location type is missing");
    assert(location.workingHours !== undefined, "Working hours is missing");
    assert(location.phone !== undefined, "Location phone number is missing");
    assert(location.address.city !== undefined, "City is missing");
    assert(location.address.postalCode !== undefined, "Postal code is missing");
    assert(location.address.countryCode !== undefined, "Country code is missing");
    assert(location.address.stateCode !== undefined, "State code is missing");
    assert(location.address.street1 !== undefined, "Address line 1 is missing");
    assert(location.address.geocode.lat !== undefined, "Latitude is missing");
    assert(location.address.geocode.long !== undefined, "Longitude is missing");
}

async function verifyDateTime(dateAndTime){
    assert(dateAndTime.date !== undefined, "Date is missing");
    assert(dateAndTime.time !== undefined, "Time is missing");
}

async function verifyVendor(vendor){
    assert(vendor.name !== undefined, "Vendor name is missing");
    assert(vendor.code !== undefined, "Vendor code is missing");
}

async function verifyRate(car) {
    assert(car.carCosts.perDay, `Price per day is missing ${JSON.stringify(car)}`)
    assert(car.carCosts.perDay.amount, `Price per day is missing ${JSON.stringify(car)}`);
    //assert(car.carCosts.extraHour, `Extra hour price is missing ${JSON.stringify(car)}`);
   // assert(car.carCosts.extraHour.amount, `Extra hour price is missing + JSON.stringify(car)`);
    // assert(car.carCosts.extraDay.amount !== undefined, "Extra day price is missing");
    assert(car.totalCost.primary.amount, `Estimated Total is missing ${JSON.stringify(car)}`);
    assert(car.taxes.primary.amount, `Taxes is missing ${JSON.stringify(car)}`);
    await verifyTotalCost(car)
}

async function verifyTotalCost(car) {
    let days = getDays(car)
    let hours = getHours(car)
    let totalCost = Math.round((car.carCosts.perDay.amount * days + car.taxes.primary.amount) * 100)/100
        //car.carCosts.extraHour.amount * hours
    assert(totalCost === car.totalCost.primary.amount, `Expected: ${totalCost} (cost per day ${car.carCosts.perDay.amount} * ${days} days + ${car.taxes.primary.amount} taxes) Actual: ${car.totalCost.primary.amount}\nFailed car: ${JSON.stringify(car)}`);
}

function getDays(car) {
    let pickUpDate = new Date(car.pickUp.date)
    let dropOffDate = new Date(car.dropOff.date)
    return Math.round(Math.abs((dropOffDate - pickUpDate) / (24 * 60 * 60 * 1000)))
}

function getHours(car) {
    let pickUpDate = new Date(car.pickUp.time)
    let dropOffDate = new Date(car.dropOff.time)
    return dropOffDate.getHours() - pickUpDate.getHours()
}

step("Verify that a random row in search results includes number of days, cost per day, total cost, taxes and service fees", async function () {
    let randomRow = commonUtils.calcRandomInt(scenarioStore.get("carRentalSearchCars").length-1);
    let car = scenarioStore.get("carRentalSearchCars")[randomRow];

    assert(car.dropOff !== undefined || car.dropOff.date !== undefined, "Drop off date is missing");
    assert(car.dropOff.date !== "", "Drop off date is empty");

    assert(car.pickUp !== undefined || car.pickUp.date !== undefined, "Drop off date is missing");
    assert(car.pickUp.date !== "", "Pick up date is empty");

    let noOfDays = commonUtils.calcDaysBetweenDates(car.dropOff.date, car.pickUp.date);
    assert(noOfDays > 0, "Number of days should be greater than zero");

    assert(car.carCosts !== undefined || car.carCosts.perDay !== undefined, "Cost per day is missing");
    assert(car.carCosts.perDay.amount > 0.0, "Cost per day should be greater than zero");

    assert(car.totalCost !== undefined || car.totalCost.primary !== undefined, "Total cost is missing");
    assert(car.totalCost.primary.amount > 0.0, "Total cost should be greater than zero");

    assert(car.taxes !== undefined || car.taxes.primary !== undefined, "Taxes is missing");
    assert(car.taxes.primary.amount > 0.0, "Taxes should be greater than zero");

    assert(car.serviceFee !== undefined, "Service fee is missing");
});

step("Verify that a random row in search results has matching number of days, cost per day, taxes, correct calculation for total rental cost and total cost (incl. taxes & fees)", async function () {
    let numDaysInput = parseInt(scenarioStore.get("carRentalInputNumDays"));
    let randomRow = commonUtils.calcRandomInt(scenarioStore.get("carRentalSearchCars").length-1);
    let car = scenarioStore.get("carRentalSearchCars")[randomRow];
   
    let noOfDays = commonUtils.calcDaysBetweenDates(car.dropOff.date, car.pickUp.date);
    assert(noOfDays == numDaysInput, `Number of days returned should match the input number of days. Returned: ${noOfDays} Input: ${numDaysInput}`);

    assert(car.carCosts.perDay.amount > 0.0, `Cost per day ${car.carCosts.perDay.amount} should be greater than zero`);

    let totalRentalCost = car.carCosts.perDay.amount * noOfDays;
    assert(totalRentalCost > 0.0, `Total rental cost ${totalRentalCost} should be greater than zero`);

    assert(car.taxes.primary.amount > 0.0, `Taxes ${car.taxes.primary.amount} should be greater than zero`);

    let totalCost = (totalRentalCost + car.taxes.primary.amount + (car.serviceFee || 0.0)).toFixed(2);
    console.log(`Total Rental Cost: ${totalRentalCost} \n Taxes: ${car.taxes.primary.amount}\n Service Fee: ${(car.serviceFee || 0.0)}\n Calc. Cost: ${totalCost}`);
    assert(car.totalCost.primary.amount == totalCost, `Total cost returned should match calculated total cost. Returned: ${car.totalCost.primary.amount} Calculated: ${totalCost}`);
});
