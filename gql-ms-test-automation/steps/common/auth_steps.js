"use strict";

const reqBuilder = require("../../helpers/common/auth_request_builders.js");
const commonUtils = require("../../helpers/common/common_utils");
const assert = require("assert");
var scenarioStore = gauge.dataStore.scenarioStore;
step("Request login with credentials for <user>", async function (user) {
    scenarioStore.put("userDetails", commonUtils.userDetails(user));
    let siteLookupResponse = await reqBuilder.siteLookUpApi(scenarioStore.get("userDetails"));
    if (siteLookupResponse.data.data && !siteLookupResponse.data.errors) {
        console.log(`Token: ${siteLookupResponse.data.data.loginByCredentials.token}`);
        scenarioStore.put('authToken', siteLookupResponse.data.data.loginByCredentials.token);
    }
});

step("Login to graphql", async function () {
    let response = await reqBuilder.loginGraphqlApi();
    scenarioStore.put("isLoginSuccessful", response.data.data && !response.data.errors);
});

step("Verify login success for user <user>", async function (user) {
    let userDetails = scenarioStore.get("userDetails");
    assert(userDetails.username === user && gauge.dataStore.scenarioStore.get("isLoginSuccessful"), "Could not login into Graphql");
});

step("Verify login failure using <type>", async function (type) {
    let siteLookupResponse = scenarioStore.get("siteLookUpApiResponse");
    if (type === "bad password") {
        assert(!siteLookupResponse.data && siteLookupResponse.errors, "Login to graphql successful with invalid user.");
        assert(!scenarioStore.get("isLoginSuccessful"), "Login to graphql successful with invalid user.");
    }
    else if (type === "bad token") {
        let invalidToken = `${scenarioStore.get('authToken')}badtoken`;
        scenarioStore.put('authToken', invalidToken);
        let response = await reqBuilder.loginGraphqlApi();
        assert(response.data.errors[0].extensions.response.body.errors[0].message, "Login to graphql successful with invalid token.");
    }
});

