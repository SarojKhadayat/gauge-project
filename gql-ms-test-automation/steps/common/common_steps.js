const { createItinerary, bookItinerary, getOrders, getTrip, cancelTrip, modifyTrip, updateTripName } = require("../../helpers/common/common_request_builders");
const assert = require("assert");
const { generateTripName } = require("../../helpers/common/common_utils");
var scenarioStore = gauge.dataStore.scenarioStore;

step("Create itinerary", async function () {
    let getCreateItineraryResponse = await createItinerary();
    assert((getCreateItineraryResponse.data.data || !getCreateItineraryResponse.data.errors), "Itinerary creation failed with errors.");
    assert((getCreateItineraryResponse.data.data.createItinerary), "Itinerary creation failed with no response.");
    scenarioStore.put("itineraryId", getCreateItineraryResponse.data.data.createItinerary.id);
    console.log(`ItineraryID: ${scenarioStore.get("itineraryId")}`);
});

step("Book <tripType> trip and verify booking is completed", async function (tripType) {
    let selectedTrip = scenarioStore.get("selectedTrip");
    let bookingId = "";
    if (selectedTrip) {
        bookingId = selectedTrip.processId.toString();
    }
    let userDetails = scenarioStore.get("userDetails");
    let getBookItineraryResponse = await bookItinerary(tripType, scenarioStore.get("itineraryId"), bookingId, userDetails.creditCardId);
    assert((getBookItineraryResponse.data.data && !getBookItineraryResponse.data.errors), `The ${tripType} booking failed with errors.`);
    assert((getBookItineraryResponse.data.data.bookItinerary.success), `The ${tripType} booking was not successful.`);
});

step("Get upcoming trips", async function () {
    let response = await getOrders();
    let upcomingTrips = response.data.data.orders.transactionGroups;
    assert(upcomingTrips.length > 0, "No upcoming trips found.");
    scenarioStore.put("upcomingTrips", upcomingTrips);
});

step("Select and get the first trip", async function () {
    let upcomingTrips = scenarioStore.get("upcomingTrips");
    let selectedTrip = upcomingTrips[0];
    var getSelectedTrip = await getTrip(selectedTrip, scenarioStore.get("userDetails").companyId);
    assert(getSelectedTrip.data.data && getSelectedTrip.data.data.trip, "The first trip could not be retrieved.");
    scenarioStore.put("selectedTrip", getSelectedTrip.data.data.trip);
});

step("Select and get the trip with name", async function () {
    let upcomingTrips = scenarioStore.get("upcomingTrips");
    let tripName = scenarioStore.get("tripName");
    let selectedTrip = upcomingTrips.find(trip => trip.name === tripName);
    assert(selectedTrip, `The trip with name "${tripName}" could not be found in upcoming trips.`);
    var getSelectedTrip = await getTrip(selectedTrip, scenarioStore.get("userDetails").companyId);
    assert(getSelectedTrip.data.data && getSelectedTrip.data.data.trip, `The trip with name ${tripName} could not be retrieved.`);
    scenarioStore.put("selectedTrip", getSelectedTrip.data.data.trip);
});
step("Cancel and verify trip (booking) is cancelled", async function () {
    let selectedTrip = scenarioStore.get("selectedTrip");
    var response = await cancelTrip(selectedTrip);
    assert(response.data.data.cancelTrip && response.data.data.cancelTrip.success, "Trip (booking) could not be cancelled.");
});

step("Modify trip", async function () {
    let processId = scenarioStore.get("selectedTrip").processId;
    let response = await modifyTrip(processId);
    assert(response.data.data.reactivateItinerary && response.data.data.reactivateItinerary.success, "Trip could not be modified.");
});

step("Update trip name", async function () {
    let tripName = generateTripName();
    let response = await updateTripName(tripName, scenarioStore.get("itineraryId"));
    assert(response.data.data && response.data.data.updateTripName.success, `Trip name with name ${tripName} could not be updated.`);
    scenarioStore.put("tripName", tripName);
});