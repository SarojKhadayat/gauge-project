# Microservices Test Automation

A collection of automated tests targeting the non-UI calls for the microservices platform. All calls will originate from the GraphQL endpoints, which will in turn call the appropriate microservices calls and which in turn might call appropriate legacy backend calls.

## Getting Started for Developers

If you are a developer who wants to contribute to the project, please follow the steps in this section.

### Pre-Requisites

- [Download](https://nodejs.org/en/download/), install Node.js and setup the path
- Install [Gauge](https://docs.gauge.org/getting_started/installing-gauge.html?os=macos&language=javascript&ide=vscode) globally
- Install [VSCode](https://code.visualstudio.com/)
- Install [Gauge extension](https://marketplace.visualstudio.com/items?itemName=getgauge.gauge) for VSCode 

### Clone the repo (first time only)

- Clone the repo by:

**If you are using git from the terminal**

    ```
    git clone git@gitlab.com:deem-devops/[path/project_name].git
    cd gql-ms-test-automation
    ```

**If you are using SourceTree**

- Connect the `gql-ms-test-automation` project to SourceTree
- Get the latest code for the `gql-ms-test-automation` project
    - Click the Pull button in the toolbar
    - Select the remote repository to pull from
    - Select the remote branch `master` to pull changes from
    - Click OK
- cd `<path you downloaded project code>`

### Project Setup 

- Open the code in VSCode

    ```
    code .
    ```

- Install dependencies

    ```
    npm install
    ```

- Run all tests

    ```
    npm run test
    # OR
    gauge run --log-level "error"
    ```

You should see all the tests running and they should all be green.

### Code Contributions

To be able to contribute, you need to follow these steps:

- Get latest from `master` branch

    ```
    git pull origin master
    ```

- Create a new feature branch

    ```
    git checkout -b <feature_branch_name>
    ```

- Work on your changes
- Run all tests to make sure every test is green

- Ready your feature branch to do a PR:
    - Commit all your changes
        ```
        git add <files that you have changed separated by space>
        git commit -m "commit message"
        git status
        ```
    - Merge latest changes from `master` back into your feature branch
        ```
        # switch branch to master
        git checkout master

        # get latest code from master
        git pull origin master
        
        # switch branch to your feature branch
        git checkout <feature_branch_name>
        ```
        - If you **HAVE NOT** yet pushed your feature branch to Gitlab, then:
            ```
            # rebase latest changes from master
            git rebase master
            ```
            **Note**: This operation does not create a Merge commit but chronologically orders the commit history.
        - If you **HAVE** pushed your feature branch to Gitlab, then:
            ```
            # merge latest changes from master
            git merge --no-ff master
            ```
            **Note**: This operation creates a Merge commit.
    - Fix any merge conflicts
    - Run all tests to make sure every test is green
- Push feature branch upstream (ONLY when you are very close to done for your feature)
    ```
    git push origin <feature_branch_name>
    ```
- Go to Gitlab repo in a browser and do a PR from your `<feature_branch_name>` into `master`

### Run Tests Locally

```
# run all the tests
gauge run

# run all tests tagged with `<tag_name>`
gauge run --tags <tag_name>

# run all the specs under `hotels` folder
gauge run specs/hotels

# run tests in specific environment e.g. sustain
gauge run specs/hotels --env=sustain

# run tests with least verbosity (only errors will be displayed)
gauge run specs/hotels --log-level "error" 

# run tests with verbose console logs while development (default setting is "info")
gauge run specs/hotels
```

## Getting Started for Non-Developers

### Pre-Requisites

- Install [Docker](https://docs.docker.com/get-docker/). Based on your OS, please use the appropriate install instructions. Need Admin permissions to install.
- **Skip step** if you are not a developer
    - Install [SourceTree](https://www.sourcetreeapp.com/) - a Git UI client to pull code locally
    - **Note**: Once we start building and pushing the images to Artifactory as part of our CI pipeline, we would not need this step

### Run Tests in Docker

We have people who are not contributing code but want to just run tests. Please follow these steps to run tests in docker containers:

#### Pull latest code

- [Download](https://gitlab.com/deem-devops/test-automation/gql-ms-test-automation/-/archive/master/gql-ms-test-automation-master.zip) the latest project code
- Unzip the `gql-ms-test-automation-master.zip` locally
- Open up a terminal on your machine
- Change directory to `<path you downloaded project code>`
#### Build and Run Tests
    ```
    # build docker image
    docker build -t gql-ms-test-automation .

    # run tests
    docker run gql-ms-test-automation
    ```

## Reports

- Navigate to `reports/html-report` folder. Open the `index.html` file on your browser
- The reports can be customized and other third party plug-ins can be used in future

## Using XRay integration
XRay integration is used for reporting test results back to Jira XRay for executions within Test Plans.

### Reporting to X-ray from local your machine
The following updates need to be made within the `default.properties` file within `env/default/default.properties`

- `USE_XRAY` must be set to `true`

- `JIRA_TEST_PLAN_ID` must be populated with a valid Jira test plan id. e.g `JIRA_TEST_PLAN_ID = IOSAUTO-594`

- `XRAY_ID` and `XRAY_SECRET` must be populated with valid credentials. In order to get these - they are the same value as `JIRA_ID` and `JIRA_SECRET`, as Jira admin can get those for you. 

- You should also set `JIRA_USER_ID` to your own id. In order to get this value you can paste this url into a browser https://deem.atlassian.net/rest/api/latest/user/search?query=<yourUserName@deem.com> and get accountId from the response.

- An optional field is available if you need to update an existing test execution. `JIRA_TEST_EXECUTION_ID` can be set with a valid Jira execution id. e.g `JIRA_TEST_EXECUTION_ID = IOSAUTO-731`. _Note: If this field isn't set, the code will create a new execution ticket for the test plan id passed in._

For more information see the [wiki](https://deem.atlassian.net/wiki/spaces/EN/pages/1668186224/X-Ray+implementation) page.


## Logging test results to Splunk

- Install json-report plugin locally:

  gauge install json-report

  Test results are now also written to reports/json-report/result.json


- To upload result.json to Splunk:

  npm run postToSplunk --projectName="gql-ms-test-automation" --splunkauth="xxxx"

  See postToSplunk entry in package.json

  Cmd should return: "Successfully posted results to Splunk for Project Name: gql-ms-test-automation"

  Note:

  Any projectName can be used - each test team should select unique project names

  TODO: The splunk auth token should be stored within GitLab and referenced in above call


- Splunk query:

  index="automated_testing" projectName="gql-ms-test-automation"

  Note:

  The projectName must match that used in the previous npm upload command

- Splunk dashboard:

  https://deem.splunkcloud.com/en-GB/app/search/ios_automation_reporting?form.time.earliest=-7d%40h&form.time.latest=now&form.project=mobile-api-tests

  Select MS GQL Tests project in dropdown
